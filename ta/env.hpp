#ifndef ENV_HPP
#define ENV_HPP

#include "core.hpp"
#include "utils/log.hpp"

namespace ta
{

// Окружение
struct env
{
    struct path_set
    {
        const char* hagen_m_dictionary;
        const char* binary_dictionary;
        const char* log;
    };

    static env::path_set paths;

    static uint num_threads;

    env()
    {
        paths.hagen_m_dictionary = "/usr/local/russian_dictionary/source.txt";
        paths.binary_dictionary = "/usr/local/russian_dictionary/dict.dat";
        paths.log = "/var/log/ta/log.txt";

        ta::utils::log::set_log(paths.log);
    }

    ~env()
    {
        ta::utils::log::unset_log();
    }
};

}

#endif // ENV_HPP
