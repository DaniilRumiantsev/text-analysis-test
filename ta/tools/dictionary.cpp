#include "dictionary.hpp"

using namespace ta::grammar;
using namespace ta::tools;
using namespace ta::utils;

dictionary* dictionary::instance = nullptr;

const word dictionary::undefined = word(0,0,0);

dictionary::dictionary(const char* path_to_binary)
{
    std::ifstream s;
    s.open(path_to_binary);

    if (!s.is_open() || s.bad())
    {
        throw std::invalid_argument("Не удалось открыть двоичный файл словаря");
    }

    log::d() << TAG << "Двоичный файл словаря успешно открыт, начинается считывание данных";

    // Сначала в двоичном файле записано количество символов
    chars_count = read_big_endian(s);
    log::d() << TAG << "Всего символов: " << chars_count;
    // Затем количество слов
    words_count = read_big_endian(s);
    log::d() << TAG << "Всего словоформ: " << words_count;

    // Выделяем память под слова
    data = static_cast<unsigned char*>(operator new(chars_count));
    // Выделяем память под метаданные о словах
    meta = static_cast<word*>(operator new(words_count*sizeof(word)));

    log::d() << TAG << "Оперативная память под словарь успешно выделена: "
               << ((chars_count+words_count*sizeof(word))/(1024*1024))
               << " мб";

    // Начинаем считывать все слова
    // Каждое слово хранится в формате
    // ---------------------------------------------------------------------------------------
    //  характеристики | id слова (один для всех форм) | длина слова | значение слова
    // ---------------------------------------------------------------------------------------
    //     4 байта     |            3 байта            |    1 байт   | [длина слова] байт
    // ---------------------------------------------------------------------------------------

    // Текущий инцекс в массиве слов
    uint word_at = 0;

    log::d() << TAG << "Начинается считывание слов";

    for (uint i = 0; i < words_count; ++i)
    {
        // Если достигли конца файла, прекращаем считывание (такого не должно произойти)
        if (s.eof() || s.bad()) break;
        // Считываем данные о слове
        uint props = read_big_endian(s);
        uint code = read_big_endian(s);
        meta[i] = word(word_at, code, props);
        // получаем длину считанного слова
        uint word_length = meta[i].length();
        // Считываем само слово
        s.read(reinterpret_cast<char*>(data+word_at), word_length);
        // Получаем индекс начала следующего слова в массиве символов
        word_at += word_length;
    }

    log::d() << TAG << "Считывание слов завершено";
}

dictionary::~dictionary()
{
    operator delete(data);
    operator delete(meta);
}

uint dictionary::find(const unsigned char* begin, const unsigned char* end) const
{
    uint check_at = words_count/2, left_bound = 0, right_bound = words_count-1;
    int res;

    while (true)
    {
        res = compare(check_at, begin, end);
        if (res == 0) return check_at;
        if (left_bound == check_at || right_bound == check_at) return absent;
        if (res < 0)
        {
            left_bound = check_at;
            check_at = (right_bound+check_at)/2;
        }
        if (res > 0)
        {
            right_bound = check_at;
            check_at = (check_at+left_bound)/2;
        }
    }
}

const word& dictionary::get(uint i) const
{
    return meta[i];
}

int dictionary::compare(uint at, const unsigned char *begin, const unsigned char *end) const
{
    uint i = 0, length = meta[at].length(), data_at = meta[at].index;
    const unsigned char* p;
    int res;

    for (p = begin; p != end && p != nullptr && *p != 0 && *p != '\n' && i < length; ++i,++p)
    {
        res = utils::windows1251::compare_chars(data[data_at+i], *p, true);
        if (res != 0) return res;
    }
    return (i == length && (p == end || p==nullptr || *p == 0 || *p == '\n')) ? 0 : (i < length ? 1 : -1);
}

void dictionary::print(std::ostream& s, uint at, bool append_lf) const
{
    print(s, meta[at], append_lf);
}

void dictionary::print(std::ostream& s, const word& w, bool append_lf) const
{
    if (w.length() == 0) s << '-';
    else std::copy(data+w.index, data+w.index+w.length(), std::ostream_iterator<unsigned char>(s));
    if (append_lf) s << '\n';
}

dictionary& dictionary::get()
{
    if (instance == nullptr)
    {
        instance = new dictionary(env::paths.binary_dictionary);
    }

    return *instance;
}
