#include "analysis.hpp"
#include "statistics_thresholds.hpp"

using namespace ta::grammar;
using namespace ta::tools;
using namespace ta::utils;
using namespace std::experimental;
using namespace std::experimental::filesystem;

analysis::analysis(const char* text_characteristics, const char *comparison_results, const char *precision_results):
    text_characteristics_path(text_characteristics),
    comparison_results_path(comparison_results),
    methods_precision_path(precision_results)
{

}

void analysis::analyse_all_texts_from_path(const char *texts_path)
{

    for (uint length: lengths)
    {
        // Папка для данных анализа текста в бинарном формате
        std::string binary_path = "/"+std::to_string(length)+"/binary";
        // Папка для данных анализа текста в удобном для чтения формате
        std::string readable_path = "/"+std::to_string(length)+"/readable";
        // Папка для информации о том, какие тексты каким авторам принадлежат
        std::string authors_map_path = "/"+std::to_string(length)+"/authors_map";

        if (!create_directories(text_characteristics_path+binary_path))
        {
            log::e() << TAG << "Не удаётся создать папку " << text_characteristics_path+binary_path;
            return;
        }

        if (!create_directories(text_characteristics_path+readable_path))
        {
            log::e() << TAG << "Не удаётся создать папку " << text_characteristics_path+readable_path;
            return;
        }

        if (!create_directories(text_characteristics_path+authors_map_path))
        {
            log::e() << TAG << "Не удаётся создать папку " << text_characteristics_path+authors_map_path;
            return;
        }
    }

    // Получение характеристик каждого текста
    for (auto& p: directory_iterator(texts_path))
    {
        get_text_characteristics(p.path().c_str());
    }

    // Создание карты авторов
    for (uint i = 0; i < LENGTHS; ++i)
    {
        for (auto& author_map: authors_temp_map[i])
        {
            authors_map[i][static_cast<uint>(author_map.second.size())][author_map.first] = author_map.second;
        }
    }

    // Сохранение карты авторов на диск
    for (uint i = 0; i < LENGTHS; ++i)
    {
        for (auto& num_texts_map: authors_map[i])
        {
            std::ofstream s;
            s.open(text_characteristics_path + std::string("/") +
                   std::to_string(lengths[i]) + "/authors_map/" + std::to_string(num_texts_map.first));

            if (s.is_open() && !s.bad())
            {
                for (auto& author_map: num_texts_map.second)
                {
                    s << author_map.first << ":";
                    for (auto& text_name: author_map.second)
                    {

                        s << text_name << ';';
                    }
                    s << '\n';
                }
            }

            s.close();
        }
    }
}

void analysis::get_text_characteristics(const char *text_source)
{
    log::d() << TAG << "Начало анализа текста из " << text_source;

    text t(text_source, text::scan_mode::AUTHOR_DOT_TITLE, text::analysis::SKIP_TITLES | text::analysis::SKIP_DIALOGS);

    // Анализ текста отдельно для каждой пороговой длины
    for (uint i = 0; i < LENGTHS; ++i)
    {
        if (t.analyze(lengths[i]))
        {
            // Имя файла с текстом
            std::string text_file_name = path(text_source).filename().string();
            // Создание путей для выходных файлов
            std::string readable_path =
                    text_characteristics_path+std::string("/")+std::to_string(lengths[i])+"/readable/"+text_file_name;
            std::string binary_path =
                    text_characteristics_path+std::string("/")+std::to_string(lengths[i])+"/binary/"+text_file_name;

            // Запись результатов анализа в читаемом формате
            t.print_info_to(readable_path.c_str());
            t.print_all();
            // Запись результатов анализа в двоичном формате
            t.save_as_binary(binary_path.c_str());
            // Добавление текста в карту авторов
            add_text_to_authors_map(t, i, text_file_name);
        }
        else
        {
            log::e() << TAG << "Не удалось провести анализ текста " << text_source;
        }
    }

    log::d() << TAG << "Конец анализа текста из " << text_source;
}

void analysis::add_text_to_authors_map(ta::grammar::text &t, uint length_index, std::string text_file_name)
{
    authors_temp_map[length_index][t.get_author()].push_back(text_file_name);
}

void analysis::compare_all(bool trios_ready)
{
    if (!trios_ready)
    {
        // Создание отдельной папки для результатов сравнения каждой длины
        for (uint length: lengths)
        {
            if (!create_directory(comparison_results_path+std::string("/")+std::to_string(length)))
            {
                log::e() << TAG << "Не удаётся создать папку " << comparison_results_path << '/'<< length;
                continue;
            }

        }

        get_trios();
    }

    // Выполнение сравнений для нескольких длин

    for (uint i = 0; i < LENGTHS; ++i)
    {
        compare_all_for_length(i);
    }
}

void analysis::compare_all_for_length(uint length_index)
{
    log::d() << TAG << "Сравнение текстов длиной " << lengths[length_index] << " началось";

    std::string results = comparison_results_path+std::string("/")+std::to_string(lengths[length_index])+"/results";
    if (!create_directory(results))
    {
        log::e() << TAG << "Невозможно создать папку " << results << " для сохранения результатов";
        return;
    }

    // Загрузка текстов в оперативную память

    log::d() << TAG << "Загрузка текстов длиной " << lengths[length_index] << " в оперативную память";
    std::string texts_path = text_characteristics_path+std::string("/")+std::to_string(lengths[length_index])+"/binary";
    for (auto& p: directory_iterator(texts_path))
    {
        loaded_texts[length_index][p.path().filename()] =
                new text(p.path().c_str(), text::scan_mode::BINARY_CHARACTERISTICS);
    }

    // Считывание списка троек текстов и проведение попарных сравнений

    log::d() << TAG << "Считывание троек текстов длиной " << lengths[length_index] << " и проведение сравнения";

    uint count = 0;

    std::ifstream s;
    s.open(comparison_results_path+std::string("/")+std::to_string(lengths[length_index])+"/trios.txt");

    if (s.is_open() && !s.bad())
    {
        std::string t1, t2, t3, line;

        try
        {
            uint count_trios = 1;

            while (s >> line)
            {
                size_t semicolon1_pos = line.find(';');
                size_t semicolon2_pos = semicolon1_pos != std::string::npos
                        ? line.find(';', semicolon1_pos+1)
                        : std::string::npos;
                if (semicolon1_pos != std::string::npos && semicolon2_pos != std::string::npos)
                {
                    t1 = line.substr(0, semicolon1_pos);
                    t2 = line.substr(semicolon1_pos+1, semicolon2_pos-semicolon1_pos-1);
                    t3 = line.substr(semicolon2_pos+1);
                    try
                    {
                        std::string trio_resulsts = results+"/"+std::to_string(count_trios);
                        if (!create_directory(trio_resulsts))
                        {
                            log::e() << TAG << "Невозможно создать папку для результатов " << trio_resulsts;
                            continue;
                        }

                        text* text1 = loaded_texts[length_index].at(t1);
                        text* text2 = loaded_texts[length_index].at(t2);
                        text* text3 = loaded_texts[length_index].at(t3);
                        std::thread th1(&analysis::compare_texts, this, std::ref(*text1), std::ref(*text2), t1, t2, trio_resulsts, true);
                        std::thread th2(&analysis::compare_texts, this, std::ref(*text1), std::ref(*text3), t1, t3, trio_resulsts, false);
                        std::thread th3(&analysis::compare_texts, this, std::ref(*text2), std::ref(*text3), t2, t3, trio_resulsts, false);
                        th1.join();
                        th2.join();
                        th3.join();
                        ++count_trios;
                    }
                    catch (std::out_of_range& e)
                    {
                        log::e() << TAG << "Не удалось найти текст из тройки: " << e.what();
                    }
                    catch (std::exception& e)
                    {
                        log::e() << TAG << "Возникла непредвиденная ошибка: " << e.what();
                    }
                }
                else
                {
                    log::d() << TAG << "Не удалось считать тройку на позиции " << count+1;
                }

                ++count;

                if (count%100 == 0)
                {
                    log::d() << TAG << "Для длины " << lengths[length_index] << " " << count << " троек сравнено";
                }
            }
        }
        catch (std::exception& e)
        {
            log::e() << TAG << "Ошибка при анализе текстов длины " << lengths[length_index] << ": " << e.what();
        }
    }
    else
    {
        log::e() << TAG << "Не удалось открыть файл троек с текстами для длины " << lengths[length_index];
    }

    // Удаление текстов из оперативной памяти

    log::d() << TAG << "Удаление текстов длиной " << lengths[length_index] << " из оперативной памяти";
    for (auto& p: loaded_texts[length_index])
    {
        delete p.second;
    }
    loaded_texts[length_index].clear();

    s.close();
}

void analysis::get_trios()
{
    log::d() << TAG << "Получение троек текстов началось";

    for (uint i = 0; i < LENGTHS; ++i)
    {
        log::d() << TAG << "Получение троек текстов для длины " << lengths[i];
        load_authors_map(i);

        // Создаём массив индексов верхнего уровня карты
        std::vector<uint> num_text_indexes;
        for (auto& pair: authors_map[i])
        {
            num_text_indexes.push_back(pair.first);
        }
        if (num_text_indexes.size() < 2)
        {
            log::e() << TAG << "Невозможно сгенерировать случайные тройки текстов длины " << lengths[i];
        }

        // Генерация троек
        for (uint j = 0; j < num_comparisons[i]; ++j)
        {
            // Тройка текстов для попарного сравнения
            std::array<std::string, 3> trio;

            // (1) Выбираем случайный индекс
            uint num_texts_index = static_cast<uint>(randint(1, static_cast<int>(num_text_indexes.size()-1)));

            // (2) Выбираем случайного автора, от которого будет взято два текста
            uint author_index = static_cast<uint>(randint(
                                0, static_cast<int>(authors_map[i][num_text_indexes[num_texts_index]].size()-1)));
            uint count = 0;
            for (auto& pair: authors_map[i][num_text_indexes[num_texts_index]])
            {
                if (count == author_index)
                {
                    // (3) Выбираем два случайных текста
                    if (num_text_indexes[num_texts_index] == 2)
                    {
                        trio[0] = pair.second[0];
                        trio[1] = pair.second[1];
                    }
                    else
                    {
                        uint rand_index1 = 0, rand_index2 = 0, attempts = 0;
                        while (rand_index1 == rand_index2 && ++attempts <= 10)
                        {
                            rand_index1 = static_cast<uint>(randint(0, static_cast<int>(pair.second.size()-1)));
                            rand_index2 = static_cast<uint>(randint(0, static_cast<int>(pair.second.size()-1)));
                        }
                        if (rand_index1 == rand_index2) break;
                        trio[0] = pair.second[rand_index1];
                        trio[1] = pair.second[rand_index2];
                    }
                    break;
                }
                ++count;
            }

            // (4) Выбираем новый случайный индекс и нового случайного автора
            uint attempts = 0, new_num_texts_index = num_texts_index, new_author_index = author_index;
            while (new_num_texts_index == num_texts_index && new_author_index == author_index && ++attempts <= 10)
            {
                new_num_texts_index = static_cast<uint>(randint(1, static_cast<int>(num_text_indexes.size()-1)));
                new_author_index = static_cast<uint>(randint(
                                0, static_cast<int>(authors_map[i][num_text_indexes[num_texts_index]].size()-1)));
            }

            if (new_num_texts_index == num_texts_index && new_author_index == author_index) break;

            for (auto& pair: authors_map[i][num_text_indexes[new_num_texts_index]])
            {
                if (count == new_author_index)
                {
                    // (5) Выбираем случайный текст
                    if (num_text_indexes[new_num_texts_index] == 1)
                    {
                        trio[2] = pair.second[0];
                    }
                    else
                    {
                        trio[2] = pair.second[static_cast<uint>(randint(0, static_cast<int>(pair.second.size()-1)))];
                    }
                    break;
                }
                ++count;
            }

            // Добавление тройки в общий список
            if (trio[0].size() > 0 && trio[1].size() > 0 && trio[2].size() > 0)
            {
                bool exists = false;
                for (auto& t: trios[i])
                {
                    uint same = 0;
                    if (trio[0] == t[0] || trio[0] == t[1] || trio[0] == t[2]) ++same;
                    if (trio[1] == t[0] || trio[1] == t[1] || trio[1] == t[2]) ++same;
                    if (trio[2] == t[0] || trio[2] == t[1] || trio[2] == t[2]) ++same;
                    if (same > 1)
                    {
                        exists = true;
                        break;
                    }
                }
                if (!exists) trios[i].insert(trio);
            }
        }

        // Сохранение троек на диск

        std::ofstream s;
        s.open(comparison_results_path+std::string("/")+std::to_string(lengths[i])+"/trios.txt");
        if (!s.is_open() || s.bad())
        {
            log::e() << TAG << "Не удаётся записать список троек текстов для длины " << lengths[i] << " на диск";
        }

        for (auto& t: trios[i])
        {
            s << t[0] << ';' << t[1] << ';' << t[2] << '\n';
        }
        s.close();
    }

    log::d() << TAG << "Получение троек текстов завершено";
}

void analysis::load_authors_map(uint length_index)
{
    log::d() << TAG << "Загрузка карты авторов в оперативную память для длины " << lengths[length_index];

    std::string authors_map_path = text_characteristics_path + std::string("/") +
            std::to_string(lengths[length_index]) + std::string("/authors_map");

    for (auto& file: directory_iterator(authors_map_path))
    {
        // Каждый файл назван числом (кол-во текстов) - индекс верхнего уровня из карты авторов
        try
        {
            uint num_texts = static_cast<uint>(std::stoul(file.path().stem()));
            std::ifstream s;
            s.open(file.path().c_str());
            if (!s.is_open() || s.bad())
            {
                log::e() << TAG << "Ошибка открытия файла " + file.path().string();
                continue;
            }
            std::string line;
            while (std::getline(s, line))
            {
                // До двоеточия - имя автора
                size_t colon_pos = line.find(':');
                if (colon_pos != std::string::npos)
                {
                    std::string author = line.substr(0, colon_pos);
                    // Далле - список текстов
                    size_t next_text_pos = colon_pos + 1;
                    size_t semicolon_pos = line.find(';');
                    while (semicolon_pos != std::string::npos)
                    {
                        std::string text = line.substr(next_text_pos, semicolon_pos-next_text_pos);
                        authors_map[length_index][num_texts][author].push_back(text);
                        next_text_pos = semicolon_pos + 1;
                        semicolon_pos = line.find(';', next_text_pos);
                    }
                }
            }
            s.close();
        }
        catch (std::invalid_argument& e)
        {
            log::e() << TAG << "Невозможно считать карту авторов из " + file.path().string() << ": " << e.what();
        }
        catch (std::exception& e)
        {
            log::e() << TAG << "Ошибка чтения карты данных из " + file.path().string() << ": " << e.what();
        }
    }

    log::d() << TAG << "Загрузка карты авторов для длины " << lengths[length_index] << " завершена";
}

void analysis::compare_texts(
        ta::grammar::text &t1,
        ta::grammar::text &t2,
        std::string t1_name,
        std::string t2_name,
        std::string results_path,
        bool same_author)
{
    std::string file_path = results_path + '/' + (same_author ? 's' : 'd') + ' ' + t1_name + '-' + t2_name + ".txt";
    std::ofstream s;
    s.open((file_path));

    if (s.is_open() && !s.bad())
    {
        uint m_no = 0;

        try
        {
            for (uint i = 1; i <= 15; ++i)
            {
                if (i != 3 && i != 7 && i != 11) continue;
                auto statistics = get_statistics(t1.get_distribution_by_number(i), t2.get_distribution_by_number(i));
//                // Хи-Квадрат
//                s << ++m_no << ':' << statistics.first[0].first << ',' << statistics.first[0].second << '\n';
//                // Колмогоров-Cмирнов
//                s << ++m_no << ':' << statistics.first[1].first << ',' << statistics.first[1].second << '\n';
                // Разность распределений
                s << ++m_no << ':' << statistics.second.first << '\n';
//                // Энтропия
//                s << ++m_no << ':' << statistics.second.second << '\n';
            }

//            for (uint i = 16; i <= 18; ++i)
//            {
//                double ch1 = t1.get_primitive_by_number(i), ch2 = t2.get_primitive_by_number(i);
//                s << ++m_no << ':' << (ch1 < ch2 ? ch2 - ch1 : ch1 - ch2) << '\n';
//            }
        }
        catch (std::exception& e)
        {
            log::e() << TAG << "Возникла ошибка при сравнении текстов: " << e.what();
        }

    }

    s.close();
}

analysis::statictics analysis::get_statistics(std::map<uint, uint> &d1, std::map<uint, uint> &d2)
{
    statictics s;

    try
    {
        // Множество значений в виде объединения множеств ключей из обоих распределений
        std::set<uint> values;
        // Общее число элементов в первом и втором распределениях
        int n1 = 0, n2 = 0;

        for (auto& p: d1)
        {
            values.insert(p.first);
            n1 += p.second;
        }

        for (auto& p: d2)
        {
            values.insert(p.first);
            n2 += p.second;
        }

        // Массивы относительных частот в первом и втором распределениях
        double* p1 = new double[values.size()];
        double* p2 = new double[values.size()];

        // Сумма для вычисления Хи-Квадрат
        double chi_square_sum = 0;

        // Подсчёт относительных частот
        uint index = 0;
        for (uint value: values)
        {
            // Количество вхождений value в первом и втором распределениях
            int o1, o2;

            try
            {
                o1 = static_cast<int>(d1.at(value));
            }
            catch (std::out_of_range&)
            {
                o1 = 0;
            }

            try
            {
                o2 = static_cast<int>(d2.at(value));
            }
            catch (std::out_of_range&)
            {
                o2 = 0;
            }

            p1[index] = static_cast<double>(o1)/n1;
            p2[index] = static_cast<double>(o2)/n2;

            if (o1 != 0 || o2 != 0)
            {
                double diff = n1*o2 - n2*o1;
                chi_square_sum += (diff*diff)/(o1+o2);
            }

            ++index;
        }

        // Подсчёт расстояния между распределениями, энтропии и значения критерия Колмогорова-Смирнова
        double total_diff = 0., max_diff = 0., e1 = 0., e2 = 0.;
        for (uint i = 0; i < values.size(); ++i)
        {
            double diff = p1[i] < p2[i] ? p2[i] - p1[i] : p1[i] - p2[i];
            total_diff += diff;
            if (diff > max_diff) max_diff = diff;

            if (p1[i] > 0) e1 += std::log2(p1[i])*p1[i];
            if (p2[i] > 0) e2 += std::log2(p2[i])*p2[i];
        }

        e1 = -e1;
        e2 = -e2;

        // Запись значения статистики Хи-Квадрат
        s.first[0] = {chi_square_sum/(n1*n2), values.size()-1};
        // Запись значения статистики Колмогорова-Смирнова
        s.first[1] = {max_diff*std::sqrt(static_cast<double>(n1*n2)/(n1+n2)), values.size()-1};
        // Запись разности распределений
        s.second.first = total_diff;
        // Запись модуля разности энтропий
        s.second.second = e1 < e2 ? e2 - e1 : e1 - e2;
    }
    catch (std::exception& e)
    {
        log::e() << TAG << "При сравнении распределений произошла ошибка: " << e.what();
    }

    return s;
}

void analysis::get_methods_precision()
{
    for (uint i = 0; i < LENGTHS; ++i)
    {
        log::d() << TAG << "Получаем результаты точности методов для текстов длины " << lengths[i];

        std::string comparison_results = comparison_results_path+std::string("/")+std::to_string(lengths[i])+"/results";

        uint trio_count = 0;

        for (auto& trio_results: directory_iterator(comparison_results))
        {
            double same_author_values[NUM_METHODS] = {0};
            uint same_author_df[NUM_METHODS] = {0};
            double different_author_values[2][NUM_METHODS] = {0};
            uint different_author_df[2][NUM_METHODS] = {0};
            uint current_array_index = 0;

            for (auto& file: directory_iterator(trio_results.path().string()))
            {
                std::string file_stem = file.path().stem();

                // Считываем результаты по сравнению текстов одного автора
                if (file_stem.find('s') == 0)
                {
                    std::ifstream s;
                    std::string line;
                    s.open(file.path().c_str());
                    while (!s.eof())
                    {
                        s >> line;
                        size_t colon_pos = line.find(':');
                        if (colon_pos == std::string::npos) continue;
                        size_t k = std::stoul(line.substr(0, colon_pos));
                        if (k > NUM_METHODS) continue;

                        // Не Хи-Квадрат и не Колмогоров-Смирнов
                        if ((k%4 != 1 && k%4 != 2) || k >= 61)
                        {
                            same_author_values[k-1] = std::stod(line.substr(colon_pos+1));
                        }
                        else
                        {
                            size_t comma_pos = line.find(',');
                            if (comma_pos == std::string::npos) continue;
                            same_author_values[k-1] = std::stod(line.substr(colon_pos+1, comma_pos));
                            same_author_df[k-1] = static_cast<uint>(std::stoul(line.substr(comma_pos+1)));
                        }
                    }
                    s.close();
                }
                // Считываем результаты по сравнению текстов разных авторов
                else if (file_stem.find('d') == 0)
                {
                    if (current_array_index > 1) continue;

                    std::ifstream s;
                    std::string line;
                    s.open(file.path().c_str());
                    while (!s.eof())
                    {
                        s >> line;
                        size_t colon_pos = line.find(':');
                        if (colon_pos == std::string::npos) continue;
                        size_t k = std::stoul(line.substr(0, colon_pos));
                        if (k > NUM_METHODS) continue;

                        if ((k%4 != 1 && k%4 != 2) || k >= 61)
                        {
                            different_author_values[current_array_index][k-1] = std::stod(line.substr(colon_pos+1));
                        }
                        else
                        {
                            size_t comma_pos = line.find(',');
                            if (comma_pos == std::string::npos) continue;
                            different_author_values[current_array_index][k-1] = std::stod(line.substr(colon_pos+1, comma_pos));
                            different_author_df[current_array_index][k-1] =
                                    static_cast<uint>(std::stoul(line.substr(comma_pos+1)));
                        }
                    }
                    s.close();

                    ++current_array_index;
                }
            }

            // Считаем точность методов
            for (uint k = 0; k < NUM_METHODS; ++k)
            {
                if (same_author_values[k]==0. || different_author_values[0][k]==0. || different_author_values[1][k]==0.)
                {
                    continue;
                }

                // Не Хи-2 и не Колмогоров-Смирнов
                if ((k%4 != 0 && k%4 != 1) || k >= NUM_DISTRIBUTIONS*4)
                {
                    // +2 сравнения
                    precision_results[i][k][1] += 2;

                    // +1 правильный результат
                    if (same_author_values[k] < different_author_values[0][k])
                    {
                        ++precision_results[i][k][0];
                        if (k < NUM_DISTRIBUTIONS*4)
                        {
                            trios_success[k%4][k/4][i].insert(trio_count);
                        }
                    }

                    // +1 правильный результат
                    if (same_author_values[k] < different_author_values[1][k])
                    {
                        ++precision_results[i][k][0];
                        if (k < NUM_DISTRIBUTIONS*4)
                        {
                            trios_success[k%4][k/4][i].insert(trio_count);
                        }
                    }
                }
                else
                {
                    if (same_author_df[k] == 0 || different_author_df[0][k] == 0 || different_author_df[1][k] == 0)
                    {
                        continue;
                    }

                    // +2 сравнения
                    precision_results[i][k][1] += 2;

                    // +1 правильный результат
                    if (same_author_values[k] < different_author_values[0][k])
                    {
                        ++precision_results[i][k][0];
                        trios_success[k%4][k/4][i].insert(trio_count);
                    }

                    // +1 правильный результат
                    if (same_author_values[k] < different_author_values[1][k])
                    {
                        ++precision_results[i][k][0];
                        trios_success[k%4][k/4][i].insert(trio_count);
                    }

                    // Хи-2
                    if (k%4 == 0)
                    {
                        if (same_author_values[k] > ta::tools::chi_sqaure_table[same_author_df[k]%30000])
                        {
                            ++precision_results[i][k][2];
                        }

                        if (different_author_values[0][k]>ta::tools::chi_sqaure_table[different_author_df[0][k]%30000])
                        {
                            ++precision_results[i][k][3];
                        }

                        if (different_author_values[1][k]>ta::tools::chi_sqaure_table[different_author_df[1][k]%30000])
                        {
                            ++precision_results[i][k][3];
                        }
                    }
                    // Колмогоров-Смирнов
                    else
                    {
                        if (same_author_values[k] > ta::tools::ks_treshold)
                        {
                            ++precision_results[i][k][2];
                        }

                        if (different_author_values[0][k] > ta::tools::ks_treshold)
                        {
                            ++precision_results[i][k][3];
                        }

                        if (different_author_values[1][k] > ta::tools::ks_treshold)
                        {
                            ++precision_results[i][k][3];
                        }
                    }
                }
            }
            ++trio_count;
        }

        log::d() << TAG << "Результаты точности методов для текстов длины " << lengths[i] << " получены";
    }

    // Печать результатов точности в файл

    uint i = 0;
    for (auto& length_results: precision_results)
    {
        std::ofstream s;
        s.open(methods_precision_path+std::string("/")+std::to_string(lengths[i])+".txt");

        for (auto& arr: length_results)
        {
            s << arr[0] << '\t' << arr[1] << '\t' << arr[2] << '\t' << arr[3] << '\t'
                                << static_cast<double>(arr[0])/arr[1] << '\t'
                                << static_cast<double>(arr[2])/((arr[1]/2)) << '\t'
                                << static_cast<double>(arr[3])/(arr[1]) << '\n';
        }

        s.close();

        ++i;
    }

    // Печать соотношения результатов по разным методам относительно самого точного - сравнения векторами

    uint j = 0;
    for (auto& length_results: trios_success)
    {
        std::ofstream s;
        s.open(methods_precision_path+std::string("/")+std::to_string(lengths[j])+"meths.txt");

        for (auto& characteristic: length_results)
        {
            uint in_vectors_found[3] = {0,0,0};
            for (auto& trio: characteristic[0])
            {
                if (characteristic[2].find(trio) != characteristic[2].end())
                {
                    ++in_vectors_found[0];
                }
            }
            for (auto& trio: characteristic[1])
            {
                if (characteristic[2].find(trio) != characteristic[2].end())
                {
                    ++in_vectors_found[1];
                }
            }
            for (auto& trio: characteristic[3])
            {
                if (characteristic[2].find(trio) != characteristic[2].end())
                {
                    ++in_vectors_found[2];
                }
            }
            s << static_cast<double>(in_vectors_found[0])/characteristic[2].size() << '\t'
              << static_cast<double>(in_vectors_found[1])/characteristic[2].size() << '\t'
              << static_cast<double>(in_vectors_found[2])/characteristic[2].size() << '\n';
        }

        s.close();

        ++j;
    }

}

void analysis::get_h_precision()
{
    for (uint i = 0; i < LENGTHS; ++i)
    {
        std::string comparison_results = comparison_results_path+std::string("/")+std::to_string(lengths[i])+"/results";

        for (auto& trio_results: directory_iterator(comparison_results))
        {
            double same_author_values[NUM_METHODS] = {0};
            double different_author_values[2][NUM_METHODS] = {0};
            uint current_array_index = 0;

            for (auto& file: directory_iterator(trio_results.path().string()))
            {
                std::string file_stem = file.path().stem();

                // Считываем результаты по сравнению текстов одного автора
                if (file_stem.find('s') == 0)
                {
                    std::ifstream s;
                    std::string line;
                    s.open(file.path().c_str());
                    while (!s.eof())
                    {
                        s >> line;
                        size_t colon_pos = line.find(':');
                        if (colon_pos == std::string::npos) continue;
                        size_t k = std::stoul(line.substr(0, colon_pos));
                        if (k > NUM_METHODS) continue;

                        // Не Хи-Квадрат и не Колмогоров-Смирнов
                        if ((k%4 != 1 && k%4 != 2) || k >= 61)
                        {
                            same_author_values[k-1] = std::stod(line.substr(colon_pos+1));
                        }
                        else
                        {
                            size_t comma_pos = line.find(',');
                            if (comma_pos == std::string::npos) continue;
                            same_author_values[k-1] = std::stod(line.substr(colon_pos+1, comma_pos));
                        }
                    }
                    s.close();
                }
                // Считываем результаты по сравнению текстов разных авторов
                else if (file_stem.find('d') == 0)
                {
                    if (current_array_index > 1) continue;

                    std::ifstream s;
                    std::string line;
                    s.open(file.path().c_str());
                    while (!s.eof())
                    {
                        s >> line;
                        size_t colon_pos = line.find(':');
                        if (colon_pos == std::string::npos) continue;
                        size_t k = std::stoul(line.substr(0, colon_pos));
                        if (k > NUM_METHODS) continue;

                        if ((k%4 != 1 && k%4 != 2) || k >= 61)
                        {
                            different_author_values[current_array_index][k-1] = std::stod(line.substr(colon_pos+1));
                        }
                        else
                        {
                            size_t comma_pos = line.find(',');
                            if (comma_pos == std::string::npos) continue;
                            different_author_values[current_array_index][k-1] = std::stod(line.substr(colon_pos+1, comma_pos));
                        }
                    }
                    s.close();

                    ++current_array_index;
                }
            }

            // Считаем точность методов

            double texts_pair1[2] = {0}, texts_pair2[2] = {0};

            for (uint k = 0; k < NUM_METHODS; ++k)
            {
                if ((k != 2 && k != 6 && k != 10) ||same_author_values[k]==0. ||
                        different_author_values[0][k]==0. ||
                        different_author_values[1][k]==0.)
                {
                    continue;
                }



                // +1 правильный результат
                if (same_author_values[k] < different_author_values[0][k])
                {
                    texts_pair1[0] += h_methods_rate[i][k];
                }
                else
                {
                    texts_pair1[1] += h_methods_rate[i][k];
                }

                // +1 правильный результат
                if (same_author_values[k] < different_author_values[1][k])
                {
                    texts_pair2[0] += h_methods_rate[i][k];
                }
                else
                {
                    texts_pair2[1] += h_methods_rate[i][k];
                }

            }

            if (texts_pair1[0] != 0.)
            {
                // +1 сравнение
                h_precision_results[i][0] += 1;

                if (texts_pair1[0] > texts_pair1[1])
                {
                    // +1 правильный результат
                    h_precision_results[i][1] += 1;
                }
            }

            if (texts_pair2[0] != 0.)
            {
                // +1 сравнение
                h_precision_results[i][0] += 1;

                if (texts_pair2[0] > texts_pair2[1])
                {
                    // +1 правильный результат
                    h_precision_results[i][1] += 1;
                }
            }
        }
    }

    // Печать результатов точности в файл
    std::ofstream s;
    s.open(methods_precision_path+std::string("/h.txt"));
    for (uint i = 0; i < LENGTHS; ++i)
    {
        s << static_cast<double>(h_precision_results[i][1])/h_precision_results[i][0] << '\n';
    }
    s.close();
}

void analysis::get_n_texts(uint n)
{
    log::d() << TAG << "Получение " << n << " текстов началось";

    for (uint i = 0; i < LENGTHS; ++i)
    {
        log::d() << TAG << "Получение " << n << " текстов для длины " << lengths[i];
        load_authors_map(i);

        // Создаём массив индексов верхнего уровня карты
        std::vector<uint> num_text_indexes;
        for (auto& pair: authors_map[i])
        {
            num_text_indexes.push_back(pair.first);
        }
        if (num_text_indexes.size() < 2)
        {
            log::e() << TAG << "Невозможно сгенерировать случайные " << n << " текстов длины " << lengths[i];
        }

        // Генерация последовательностей
        for (uint j = 0; j < 1000*(n-2); ++j)
        {
            // n текстов для попарного сравнения
            std::vector<std::string> texts;

            // (1) Выбираем случайный индекс
            uint num_texts_index = static_cast<uint>(randint(1, static_cast<int>(num_text_indexes.size()-1)));

            // (2) Выбираем случайного автора, от которого будет взято два текста
            uint author_index = static_cast<uint>(randint(
                                0, static_cast<int>(authors_map[i][num_text_indexes[num_texts_index]].size()-1)));
            uint count = 0;
            for (auto& pair: authors_map[i][num_text_indexes[num_texts_index]])
            {
                if (count == author_index)
                {
                    // (3) Выбираем два случайных текста
                    if (num_text_indexes[num_texts_index] == 2)
                    {
                        texts.push_back(pair.second[0]);
                        texts.push_back(pair.second[1]);
                    }
                    else
                    {
                        uint rand_index1 = 0, rand_index2 = 0, attempts = 0;
                        while (rand_index1 == rand_index2 && ++attempts <= 10)
                        {
                            rand_index1 = static_cast<uint>(randint(0, static_cast<int>(pair.second.size()-1)));
                            rand_index2 = static_cast<uint>(randint(0, static_cast<int>(pair.second.size()-1)));
                        }
                        if (rand_index1 == rand_index2) break;
                        texts.push_back(pair.second[rand_index1]);
                        texts.push_back(pair.second[rand_index2]);
                    }
                    break;
                }
                ++count;
            }

            // (4) Выбираем новый случайный индекс и нового случайного автора
            for (uint r = 0; r < n-2; ++r)
            {
                uint attempts = 0, new_num_texts_index = num_texts_index, new_author_index = author_index;
                while (new_num_texts_index == num_texts_index && new_author_index == author_index && ++attempts <= 10)
                {
                    new_num_texts_index = static_cast<uint>(randint(1, static_cast<int>(num_text_indexes.size()-1)));
                    new_author_index = static_cast<uint>(randint(
                                    0, static_cast<int>(authors_map[i][num_text_indexes[num_texts_index]].size()-1)));
                }

                if (new_num_texts_index == num_texts_index && new_author_index == author_index) break;

                for (auto& pair: authors_map[i][num_text_indexes[new_num_texts_index]])
                {
                    if (count == new_author_index)
                    {
                        // (5) Выбираем случайный текст
                        if (num_text_indexes[new_num_texts_index] == 1)
                        {
                            if (std::find(texts.begin(), texts.end(), pair.second[0]) == texts.end())
                            {
                                texts.push_back(pair.second[0]);
                            }

                        }
                        else
                        {
                            std::string t = pair.second[static_cast<uint>(randint(0, static_cast<int>(pair.second.size()-1)))];
                            if (std::find(texts.begin(), texts.end(), t) == texts.end())
                            {
                                texts.push_back(t);

                            }
                        }
                        break;
                    }
                    ++count;
                }

            }

            // Добавление последовательности в общий список
            if (texts.size() == n)
            {
//                bool do_not_insert = false;
//                for (auto& t: n_texts[i])
//                {
//                    if (std::find(texts.begin(), texts.begin()+2, t[0]) != texts.begin()+2 && std::find(texts.begin(), texts.begin()+2, t[1]) != texts.begin()+2)
//                    {
//                        do_not_insert = true;
//                        break;
//                    }
//                }
                //if (!do_not_insert)
                n_texts[i].insert(texts);
            }
        }

        // Сохранение последовательностей на диск

        std::ofstream s;
        s.open(comparison_results_path+std::string("/")+std::to_string(lengths[i])+"/"+std::to_string(n)+".txt");
        if (!s.is_open() || s.bad())
        {
            log::e() << TAG << "Не удаётся записать список текстов для длины " << lengths[i] << " на диск";
        }

        for (auto& texts: n_texts[i])
        {
            for (auto&t : texts) s << t << ';';
            s << '\n';
        }
        s.close();
        n_texts[i].clear();
    }

    log::d() << TAG << "Получение " << n << " текстов завершено";
}

void analysis::check_h_precision(uint n)
{
    for (uint i = 0; i < LENGTHS; ++i)
    {
        compare_n_texts_for_length(n, i);
    }
}

void analysis::compare_n_texts_for_length(uint n, uint length_index)
{
    log::d() << TAG << "Сравнение текстов длиной " << lengths[length_index] << " началось";

    std::string results = comparison_results_path+std::string("/")+std::to_string(lengths[length_index])+"/"+std::to_string(n);
    if (!create_directory(results))
    {
        log::e() << TAG << "Невозможно создать папку " << results << " для сохранения результатов";
        return;
    }

    // Загрузка текстов в оперативную память

    log::d() << TAG << "Загрузка текстов длиной " << lengths[length_index] << " в оперативную память";
    std::string texts_path = text_characteristics_path+std::string("/")+std::to_string(lengths[length_index])+"/binary";
    for (auto& p: directory_iterator(texts_path))
    {
        loaded_texts[length_index][p.path().filename()] =
                new text(p.path().c_str(), text::scan_mode::BINARY_CHARACTERISTICS);
    }

    // Считывание n текстов и проведение попарных сравнений

    log::d() << TAG << "Считывание списка текстов длиной " << lengths[length_index] << " и проведение сравнения";

    uint count = 0;

    std::ifstream s;
    s.open(comparison_results_path+std::string("/")+std::to_string(lengths[length_index])+"/"+std::to_string(n)+".txt");

    if (s.is_open() && !s.bad())
    {
        std::string line;

        try
        {
            uint count_experiments = 1;

            while (s >> line)
            {
                std::vector<std::string> texts;
                std::string str = line;
                size_t semicolon_pos = str.find(';');
                do
                {
                    texts.push_back(str.substr(0, semicolon_pos));
                    if (semicolon_pos != std::string::npos) str = str.substr(semicolon_pos+1);
                    semicolon_pos = str.find(';');
                }
                while (semicolon_pos != std::string::npos);

                if (texts.size() == n)
                {
                    try
                    {
                        std::string n_resulsts = results+"/"+std::to_string(count_experiments);
                        if (!create_directory(n_resulsts))
                        {
                            log::e() << TAG << "Невозможно создать папку для результатов " << n_resulsts;
                            continue;
                        }

                        std::vector<text*> parsed_texts;
                        for (auto& t: texts) parsed_texts.push_back(loaded_texts[length_index].at(t));

                        if (parsed_texts.size() != n)
                        {

                            log::e() << TAG << "Не удалось найти все из " << n << " текстов";
                            continue;
                        }

                        compare_texts(std::ref(*parsed_texts[0]), std::ref(*parsed_texts[1]), texts[0], texts[1], n_resulsts, true);
                        for (uint x = 2; x < parsed_texts.size(); ++x)
                        {
                            compare_texts(std::ref(*parsed_texts[0]), std::ref(*parsed_texts[x]), texts[0], texts[x], n_resulsts, false);
                        }

                        ++count_experiments;
                    }
                    catch (std::out_of_range& e)
                    {
                        log::e() << TAG << "Не удалось найти текст из списка: " << e.what();
                    }
                    catch (std::exception& e)
                    {
                        log::e() << TAG << "Возникла непредвиденная ошибка: " << e.what();
                    }
                }
                else
                {
                    log::d() << TAG << "Не удалось считать список на позиции " << count+1;
                }

                ++count;

                if (count%100 == 0)
                {
                    log::d() << TAG << "Для длины " << lengths[length_index] << " " << count << " списков сравнено";
                }

                if (count > 500) break;
            }
        }
        catch (std::exception& e)
        {
            log::e() << TAG << "Ошибка при анализе текстов длины " << lengths[length_index] << ": " << e.what();
        }
    }
    else
    {
        log::e() << TAG << "Не удалось открыть файл троек с текстами для длины " << lengths[length_index];
    }

    // Удаление текстов из оперативной памяти

    log::d() << TAG << "Удаление текстов длиной " << lengths[length_index] << " из оперативной памяти";
    for (auto& p: loaded_texts[length_index])
    {
        delete p.second;
    }
    loaded_texts[length_index].clear();

    s.close();
}

void analysis::h_precision_for_n_texts(uint n)
{
    for (uint i = 0; i < LENGTHS; ++i)
    {
        std::string comparison_results = comparison_results_path+std::string("/")+std::to_string(lengths[i])+"/"+std::to_string(n);

        for (auto& n_results: directory_iterator(comparison_results))
        {
            double same_author_values[3] = {0};
            std::vector<double> different_author_values[3];

            for (auto& file: directory_iterator(n_results.path().string()))
            {
                std::string file_stem = file.path().stem();

                // Считываем результаты по сравнению текстов одного автора
                if (file_stem.find('s') == 0)
                {
                    std::ifstream s;
                    std::string line;
                    s.open(file.path().c_str());
                    while (!s.eof())
                    {
                        s >> line;
                        size_t colon_pos = line.find(':');
                        if (colon_pos == std::string::npos) continue;
                        size_t k = std::stoul(line.substr(0, colon_pos));
                        if (k > 3) continue;
                        same_author_values[k-1] = std::stod(line.substr(colon_pos+1));
                    }
                    s.close();
                }
                // Считываем результаты по сравнению текстов разных авторов
                else if (file_stem.find('d') == 0)
                {
                    std::ifstream s;
                    std::string line;
                    s.open(file.path().c_str());
                    while (!s.eof())
                    {
                        s >> line;
                        size_t colon_pos = line.find(':');
                        if (colon_pos == std::string::npos) continue;
                        size_t k = std::stoul(line.substr(0, colon_pos));
                        if (k > 3) continue;
                        different_author_values[k-1].push_back(std::stod(line.substr(colon_pos+1)));

                    }
                    s.close();
                }
            }

            // Считаем точность методов

            std::vector<double> texts_sequence(n-1,0);

            for (uint k = 0; k < 3; ++k)
            {
                if (different_author_values[k].size() != n-2) continue;

                if (same_author_values[k]==0. ||
                        different_author_values[0][k]==0. ||
                        different_author_values[1][k]==0.)
                {
                    continue;
                }

                double min_diff = 1e10;
                uint min_diff_index = different_author_values[k].size();

                for (uint r = 0; r < different_author_values[k].size(); ++r)
                {
                    if (different_author_values[k][r] < min_diff)
                    {
                        min_diff = different_author_values[k][r];
                        min_diff_index = r;
                    }
                }

                if (same_author_values[k] < min_diff)
                {
                    texts_sequence[0] += h_methods_rate[i][k*4+3];
                }
                else if (min_diff_index != different_author_values[k].size())
                {
                    texts_sequence[min_diff_index+1] += h_methods_rate[i][k*4+3];
                }

            }

            bool right = true;
            for (uint r = 1; r < texts_sequence.size(); ++r)
            {
                if (texts_sequence[r] > texts_sequence[0])
                {
                    right = false;
                    break;
                }
            }

            if (right)
            {
                ++h_precision_results[i][0];
            }
            ++h_precision_results[i][1];
        }
    }

    // Печать результатов точности в файл
    std::ofstream s;
    s.open(methods_precision_path+std::string("/h")+std::to_string(n)+".txt");
    for (uint i = 0; i < LENGTHS; ++i)
    {
        s << h_precision_results[i][0] << '\t'
          << h_precision_results[i][1] << '\t'
          << static_cast<double>(h_precision_results[i][0])/h_precision_results[i][1] << '\n';

        h_precision_results[i][0] = 0;
        h_precision_results[i][1] = 0;
    }
    s.close();
}
