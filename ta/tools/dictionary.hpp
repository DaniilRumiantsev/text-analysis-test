#ifndef DICTIONARY_HPP
#define DICTIONARY_HPP

#include "../env.hpp"
#include "../utils/utils.hpp"
#include "../grammar/word.hpp"

#include <iterator>

namespace ta
{
namespace tools
{

class dictionary
{
    // Весь словарь целиком в виде одного массива, в котором слова идут подряд в алфавитном порядке без разделителей
    unsigned char* data;
    // Количество слов в словаре
    uint words_count;
    // Кодичество символов в словаре
    uint chars_count;
    // Массив метаданных о всех словоформах из словаря
    ta::grammar::word* meta;

    // Инициализация словаря из двоичного файла на диске
    dictionary(const char* path_to_binary);
    ~dictionary();

    // Экземпляр словаря
    static dictionary* instance;

public:

    // обозначение позиции того, что искомое слово не найдено
    static const uint absent = 0xFFFFFFFF;
    // Специальное значение - неопределённое слово
    static const ta::grammar::word undefined;

    // . Возвращает -1 в случае, если слово не найдено
    /**
     * @brief find Найти слово в словаре и вернуть его индекс из массива метаданных
     * @param begin Указатель на начало искомого слова в памяти
     * @param end Указатель на байт, следующий за последним символом искомого слова
     * @return Индекс слова в словаре или -1 в случае, если слово не найдено
     */
    uint find(const unsigned char* begin, const unsigned char* end) const;

    /**
     * @brief get Возвращает ссылку на слово из словаря по индексу
     * @param i Индекс
     * @return Ссылка на слово из словаря
     */
    const grammar::word &get(uint i) const;

    /**
     * @brief compare Проверяет на равенство без учёта регистра слово из словаря по указаному индексу и слово,
     * заданное массивом символов
     * @param at Индекс слова в словаре, с которым будем производить сравнение
     * @param begin Указатель на начало слова в памяти
     * @param end Указатель на байт, следующий за последним символом слова
     * @return -1, если слово из словаря идёт ранее; 0, если слова равны; 1, если слово из массива идёт позже
     */
    int compare(uint at, const unsigned char* begin, const unsigned char* end) const;

    /**
     * @brief print Печатает слово по индексу из словаря в указанный поток
     * @param s Поток для печати слова
     * @param at Индекс слова из словаря
     * @param append_lf Нужно ли делать перевод строки
     */
    void print(std::ostream& s, uint at, bool append_lf = true) const;

    /**
     * @brief print Печатает слово по значению из словаря в указанный поток
     * @param s Поток для печати слова
     * @param w Объекта слова
     * @param append_lf Нужно ли делать перевод строки
     */
    void print(std::ostream& s, const ta::grammar::word& w, bool append_lf = true) const;

    /**
     * @brief get Получает экземпляр словаря
     * @return Ссылка на экзампляр словаря
     */
    static dictionary& get();
};

}
}



#endif // DICTIONARY_HPP
