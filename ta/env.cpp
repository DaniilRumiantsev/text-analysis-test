#include "env.hpp"

ta::env::path_set ta::env::paths{nullptr,nullptr,nullptr};
uint ta::env::num_threads = std::max(1u, std::thread::hardware_concurrency());
