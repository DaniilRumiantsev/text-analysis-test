#ifndef TYPES_HPP
#define TYPES_HPP

#define TAG __FUNCTION__ << ": "

#include <cstdint>
#include <cstring>
#include <exception>
#include <fstream>
#include <iostream>
#include <mutex>
#include <sstream>
#include <thread>

namespace ta
{

// все беззнаковые числовые значения представлены типом uint32_t
using uint = std::uint32_t;

// Символы будут храниться как unsigned char
using ustring = std::basic_string<unsigned char>;
using uistringstream = std::basic_istringstream<unsigned char>;
using uostringstream = std::basic_ostringstream<unsigned char>;

}

#endif // TYPES_HPP
