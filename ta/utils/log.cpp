#include "log.hpp"

using namespace ta::utils;

log* log::instance = nullptr;
std::mutex log::m = std::mutex();

log::log(const char* filename)
    : file_stream(std::ofstream()), error_stream(std::cerr)
{
    file_stream.open(filename);
    is_initialized = file_stream.is_open() && !file_stream.bad();

    if (!is_initialized)
    {
        error_stream << "Лог не инициализирован, логирование в стандартный поток ошибок\n";
    }
}

log::~log()
{
    if (file_stream.is_open()) file_stream.close();
}

std::ostream& log::get_stream()
{
    if (is_initialized)
    {
        return file_stream;
    }
    else
    {
        return error_stream;
    }
}

void log::set_log(const char *path)
{
    if (instance == nullptr)
    {
        instance = new log(path);
    }
}

std::ostream& log::d()
{
    std::lock_guard<std::mutex> guard(m);
    std::time_t t = std::time(nullptr);
    instance->get_stream() << std::put_time(std::localtime(&t), "\n[%d.%m.%Y %H:%M:%S] ");
    instance->get_stream().flush();
    return instance->get_stream();
}

std::ostream& log::e()
{
    std::time_t t = std::time(nullptr);
    instance->get_stream() << std::put_time(std::localtime(&t), "\n[%d.%m.%Y %H:%M:%S] ");
    instance->get_stream() << "=== ОШИБКА === ";
    instance->get_stream().flush();
    return instance->get_stream();
}

void log::unset_log()
{
    if (instance != nullptr) delete instance;
}
