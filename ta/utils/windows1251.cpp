#include "windows1251.hpp"

int ta::utils::windows1251::compare_chars(unsigned char a, unsigned char b, bool yo_e_does_not_matter)
{
    if (a == b) return 0;

    // Если a или b являются буквами, сравниваем лексикографически без учёта регистра
    if ((ENCODING[a]&CHAR_TYPES::LETTER) && (ENCODING[b]&CHAR_TYPES::LETTER))
    {
        // Приводим каждую букву к нижнему регистру при необходимости

        if (ENCODING[a]&CHAR_TYPES::ENGLISH_UPPERCASE_LETTER)
        {
            a += 32;
        }
        else if (ENCODING[a]&CHAR_TYPES::RUSSIAN_UPPERCASE_LETTER)
        {
            if (a == 0xA8) a = 0xB8;
            else a += 32;
        }

        if (ENCODING[b]&CHAR_TYPES::ENGLISH_UPPERCASE_LETTER)
        {
            b += 32;
        }
        else if (ENCODING[b]&CHAR_TYPES::RUSSIAN_UPPERCASE_LETTER)
        {
            if (b == 0xA8) a = 0xB8;
            else b += 32;
        }

        // Если буквы одинаковы, проложить сравнивать следующие
        if (a == b) return 0;
        // Если е и ё не различимы
        if (yo_e_does_not_matter)
        {
            if ((a == 0xB8 && b == 0xE5) || (b == 0xB8 && a == 0xE5)) return 0;
        }

        // Если a = ё, то a меньше b только если b >= ж
        if (a == 0xB8) return b >= 0xE6 ? -1 : 1;
        // Если b = ё, то a меньше b только если a <= е
        if (b == 0xB8) return a <= 0xE5 ? -1 : 1;
    }

    // Иначе сравниваем как обычно
    return a < b ? -1 : 1;
}
