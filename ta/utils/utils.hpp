#ifndef UTILS_HPP
#define UTILS_HPP

#include "../grammar/grammar.hpp"
#include "../grammar/word.hpp"

#include <algorithm>
#include <iostream>
#include <fstream>
#include <vector>

namespace ta
{
namespace utils
{

/**
 * @brief parse_hagen_m_dictionary Преобразует словарь М. Хагена (http://www.speakrus.ru/dict/hagen-details.txt) из
 * текстового формата в двоичныый формат меньшего объёма для дальнейшей выгрузки в оперативную память
 * @param source Путь к оригинальному .txt-файлу словаря
 * @param dest Путь к выходному файлу, в который будут записаны двоичные данные
 */
void parse_hagen_m_dictionary(const char* source, const char* dest);

/**
 * @brief parse_hagen_m_morphology Преобразует набор морфологических характеристик из буквенной интерпритации в
 * компактную числовую
 * @param characteristics Буквенная интерпретация морфологических характеристик слова из словаря М. Хагена
 * @return Числовая интерпретация характеристик
 */
uint parse_hagen_m_morphology(ustring characteristics);

/**
 * @brief write_big_endian Запись 4-байтного числа в поток в нотации Big Endian
 * @param value Число для записи
 * @param s Выходной поток
 */
void write_big_endian(uint value, std::ostream& s);

/**
 * @brief read_big_endian Считывает 4 байта из потока как Big Endian число
 * @param s поток
 * @return 4-байтовое беззнаковое число
 */
uint read_big_endian(std::istream& s);

}
}

#endif // UTILS_HPP
