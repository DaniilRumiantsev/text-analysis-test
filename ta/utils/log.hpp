#ifndef LOG_HPP
#define LOG_HPP

#include "../core.hpp"

#include <ctime>
#include <iomanip>

namespace ta
{
namespace utils
{
class log
{
    std::ofstream file_stream;
    std::ostream& error_stream;
    bool is_initialized;

    /**
     * @brief log Создёт журнал и связвает его с указанным файлом
     * @param file_name Путь к файлу, в котором разместится журнал
     */
    log(const char* file_name);

    ~log();

    /**
     * @brief get_stream Возвращает поток, связанный с записью в журнал
     * @return Объект потока
     */
    std::ostream& get_stream();

    static log* instance;
    static std::mutex m;

public:

    /**
     * @brief set_log Устанавливает файл для ведения журнала. Вызывает конструктор
     * @param path
     */
    static void set_log(const char* path);

    /**
     * @brief d Возвращает поток для записи сообщения в журнал
     * @return Объект потока
     */
    static std::ostream& d();

    /**
     * @brief e Возвращает поток для записи сообщения в журнал и помечает сообщение как сообщение об ошибке
     * @return Объект потока
     */
    static std::ostream& e();

    /**
     * @brief unset_log Отвязывает поток журнала от файла на диске
     */
    static void unset_log();



};
}
}

#endif // LOG_HPP
