#include "utils.hpp"

using namespace ta;
using namespace ta::grammar;

uint ta::utils::parse_hagen_m_morphology(ustring characteristics)
{
    // Характеристиуи в виде структуры
    word_characteristics wc;

    // Какие характеристики требуются взависимости от части речи
    bool need_case_t = false;             // Падеж
    bool need_tense = false;              // Время
    bool need_type = false;               // Тип
    bool need_gender = false;             // Род
    bool need_quantity = false;           // Число
    bool need_completeness = false;       // Совершённость/несовершённость
    bool need_transitiveness = false;     // Переходность
    bool need_personality = false;        // Лицо
    bool need_degree = false;             // Степень
    bool need_selfness = false;           // Наличие возвратного суффикса "ся"
    bool need_imperativeness = false;     // Налииче повелительного наклонения
    bool need_shortness = false;          // Краткое или нет
    bool need_passive_voice = false;      // Страдательное или нет

    // Сокращения частей речи в словаре М. Хагена
    ustring intro{u(A::v), u(A::v), u(A::o), u(A::d)};
    ustring verb{u(A::g), u(A::l)};
    ustring verb_participle{u(A::d), u(A::e), u(A::e), u(A::p)};
    ustring interjection{u(A::m), u(A::e), u(A::zh), u(A::d)};
    ustring pronoun{u(A::m), u(A::e), u(A::s), u(A::t)};
    ustring adverb{u(A::n), u(A::a), u(A::r)};
    ustring predicate{u(A::p), u(A::r), u(A::e), u(A::d), u(A::i), u(A::k)};
    ustring preposition{u(A::p), u(A::r), u(A::e), u(A::d), u(A::l)};
    ustring adjective{u(A::p), u(A::r), u(A::l)};
    ustring participle{u(A::p), u(A::r), u(A::ch)};
    ustring conjunction{u(A::s), u(A::o), u(A::yu), u(A::z)};
    ustring noun{u(A::s), u(A::u), u(A::sch)};
    ustring particle{u(A::ch), u(A::a), u(A::s), u(A::t)};
    ustring number{u(A::ch), u(A::i), u(A::s), u(A::l)};

    // Сокращения характеристик в словаре М. Хагена
    ustring case_t_nominative{u(A::i), u(A::m)};
    ustring case_t_genetive{u(A::r), u(A::o), u(A::d)};
    ustring case_t_dative{u(A::d), u(A::a), u(A::t)};
    ustring case_t_acussative{u(A::v), u(A::i), u(A::n)};
    ustring case_t_instrumental{u(A::t), u(A::v)};
    ustring case_t_prepositional{u(A::p), u(A::r)};
    ustring case_t_partitive{u(A::p), u(A::a), u(A::r), u(A::t)};
    ustring case_t_countable{u(A::s), u(A::ch), u(A::e), u(A::t)};
    ustring case_t_local{u(A::m), u(A::e), u(A::s), u(A::t)};
    ustring case_t_callable{u(A::z), u(A::v), u(A::a), u(A::t)};

    ustring gender_male{u(A::m), u(A::u), u(A::zh)};
    ustring gender_female{u(A::zh), u(A::e), u(A::n)};
    ustring gender_common{u(A::o), u(A::b), u(A::sch)};

    ustring quantity_singular{u(A::e), u(A::d)};
    ustring quantity_plural{u(A::m), u(A::n)};

    ustring completeness_complete{u(A::s), u(A::o), u(A::v)};
    ustring completeness_incomplete{u(A::n),u(A::e),u(A::s),u(A::o),u(A::v)};
    ustring completeness_both{u('2'), u(A::v), u(A::i), u(A::d)};

    ustring transitiveness_transitive{u(A::p),u(A::e),u(A::r),u(A::e),u(A::h)};
    ustring transitiveness_intransitive{u(A::n),u(A::e),u(A::p),u(A::e),u(A::r)};
    ustring transitiveness_both{u(A::p),u(A::e),u(A::r),u('/'),u(A::n),u(A::e)};

    ustring selfness{u(A::v),u(A::o),u(A::z)};

    ustring tense_past{u(A::p), u(A::r), u(A::o), u(A::sh)};
    ustring tense_present{u(A::n), u(A::a), u(A::s), u(A::t)};
    ustring tense_future{u(A::b), u(A::u), u(A::d)};
    ustring tense_infinitive{u(A::i), u(A::n), u(A::f)};

    ustring personality_first{u('1'), u('-'), u(A::e)};
    ustring personality_second{u('2'), u('-'), u(A::e)};
    ustring personality_third{u('3'), u('-'), u(A::e)};

    ustring imperativeness{u(A::p),u(A::o),u(A::v)};

    ustring pronoun_adverblike{u(A::n),u(A::a),u(A::r)};
    ustring pronoun_adjectivelike{u(A::p),u(A::r),u(A::l)};
    ustring pronoun_nounlike{u(A::s),u(A::u),u(A::sch)};

    ustring adverb_question{u(A::v),u(A::o),u(A::p), u(A::r)};
    ustring adverb_condition_time{
     u(A::o),u(A::b),u(A::s),u(A::t),u(' '),u(A::v),u(A::r),u(A::e),u(A::m)
    };
    ustring adverb_condition_place{
     u(A::o),u(A::b),u(A::s),u(A::t),u(' '),u(A::m),u(A::e),u(A::s),u(A::t),u(A::a)
    };
    ustring adverb_condition_direction{
     u(A::o),u(A::b),u(A::s),u(A::t),u(' '),u(A::n),u(A::a),u(A::p),u(A::r)
    };
    ustring adverb_condition_reason{
     u(A::o),u(A::b),u(A::s),u(A::t),u(' '),u(A::p),u(A::r),u(A::i),u(A::ch),u(A::i),u(A::n)
    };
    ustring adverb_condition_purpose{
     u(A::o),u(A::b),u(A::s),u(A::t),u(' '),u(A::ts),u(A::e),u(A::l),u(A::soft_sign)
    };
    ustring adverb_definition_quality{
     u(A::o),u(A::p),u(A::r),u(A::e),u(A::d),u(' '),u(A::k),u(A::a),u(A::ch)
    };
    ustring adverb_definition_way{
     u(A::o),u(A::p),u(A::r),u(A::e),u(A::d),u(' '),u(A::s),u(A::p),u(A::o),u(A::s)
    };
    ustring adverb_definition_degree{
     u(A::o),u(A::p),u(A::r),u(A::e),u(A::d),u(' '),u(A::s),u(A::t),u(A::e),u(A::p)
    };

    ustring degree_exessive{u(A::p),u(A::r),u(A::e),u(A::v)};
    ustring degree_comparable{u(A::s),u(A::r),u(A::a),u(A::v),u(A::n)};

    ustring shortness{u(A::k), u(A::r), u(A::a), u(A::t)};

    ustring passive_voice{u(A::s), u(A::t), u(A::r), u(A::a), u(A::d)};

    ustring number_quantitive{u(A::k), u(A::o), u(A::l)};
    ustring number_collective{u(A::s), u(A::o), u(A::b), u(A::i), u(A::r)};
    ustring number_ordering{u(A::p), u(A::o), u(A::r), u(A::ya), u(A::d)};
    ustring number_unstated{u(A::n), u(A::e), u(A::o), u(A::p), u(A::r)};

    // Если это вводное слово
    if (characteristics.find(intro) == 1)
    {
        wc.part_of_speech = static_cast<uint>(PARTS_OF_SPEECH::INTRO);
    }
    // Если это глагол
    else if (characteristics.find(verb) == 1)
    {
        wc.part_of_speech = static_cast<uint>(PARTS_OF_SPEECH::VERB);
        need_tense = true;
        need_completeness = true;
        need_transitiveness = true;
        need_gender = true;
        need_quantity = true;
        need_personality = true;
        need_selfness = true;
        need_imperativeness = true;
    }
    // Если это деепричастие
    else if (characteristics.find(verb_participle) == 1)
    {
        wc.part_of_speech = static_cast<uint>(PARTS_OF_SPEECH::VERB_PARTICIPLE);
        need_tense = true;
        need_completeness = true;
        need_transitiveness = true;
        need_selfness = true;
    }
    // Если это междометие
    else if (characteristics.find(interjection) == 1)
    {
        wc.part_of_speech = static_cast<uint>(PARTS_OF_SPEECH::INTERJECTION);
    }
    // Если это местоимение
    else if (characteristics.find(pronoun) == 1)
    {
        wc.part_of_speech = static_cast<uint>(PARTS_OF_SPEECH::PRONOUN);
        need_type = true;
        need_case_t = true;
        need_gender = true;
        need_quantity = true;
    }
    // Если это наречие
    else if (characteristics.find(adverb) == 1)
    {
        wc.part_of_speech = static_cast<uint>(PARTS_OF_SPEECH::ADVERB);
        need_type = true;
        need_degree = true;
    }
    // Если это предикат
    else if (characteristics.find(predicate) == 1)
    {
        wc.part_of_speech = static_cast<uint>(PARTS_OF_SPEECH::PREDICATE);
    }
    // Если это предлог
    else if (characteristics.find(preposition) == 1)
    {
        wc.part_of_speech = static_cast<uint>(PARTS_OF_SPEECH::PREPOSITION);
        need_case_t = true;
    }
    // Если это прилагательное
    else if (characteristics.find(adjective) == 1)
    {
        wc.part_of_speech = static_cast<uint>(PARTS_OF_SPEECH::ADJECTIVE);
        need_case_t = true;
        need_gender = true;
        need_quantity = true;
        need_degree = true;
        need_shortness = true;
    }
    // Если это причастие
    else if (characteristics.find(participle) == 1)
    {
        wc.part_of_speech = static_cast<uint>(PARTS_OF_SPEECH::PARTICIPLE);
        need_case_t = true;
        need_gender = true;
        need_quantity = true;
        need_passive_voice = true;
        need_shortness = true;
        need_selfness = true;
        need_tense = true;
        need_completeness = true;
        need_transitiveness = true;
    }
    // Если это союз
    else if (characteristics.find(conjunction) == 1)
    {
        wc.part_of_speech = static_cast<uint>(PARTS_OF_SPEECH::CONJUNCTION);
    }
    // Если это существительное
    else if (characteristics.find(noun) == 1)
    {
        wc.part_of_speech = static_cast<uint>(PARTS_OF_SPEECH::NOUN);
        need_case_t = true;
        need_gender = true;
        need_quantity = true;
    }
    // Если это частица
    else if (characteristics.find(particle) == 1)
    {
        wc.part_of_speech = static_cast<uint>(PARTS_OF_SPEECH::PARTICLE);
    }
    // Если это числительное
    else if (characteristics.find(number) == 1)
    {
        wc.part_of_speech = static_cast<uint>(PARTS_OF_SPEECH::NUMBER);
        need_case_t = true;
        need_gender = true;
        need_quantity = true;
        need_type = true;
    }

    // Если необходимо определить падеж
    if (need_case_t)
    {
        if (characteristics.find(u(' ')+case_t_nominative) != std::string::npos)
        {
            wc.case_t = static_cast<uint>(CASE::NOMINATIVE);
        }
        else if (characteristics.find(u(' ')+case_t_genetive) != std::string::npos)
        {
            wc.case_t = static_cast<uint>(CASE::GENETIVE);
        }
        else if (characteristics.find(u(' ')+case_t_dative) != std::string::npos)
        {
            wc.case_t = static_cast<uint>(CASE::DATIVE);
        }
        else if (characteristics.find(u(' ')+case_t_acussative) != std::string::npos)
        {
            wc.case_t = static_cast<uint>(CASE::ACUSSATIVE);
        }
        else if (characteristics.find(u(' ')+case_t_instrumental) != std::string::npos)
        {
            wc.case_t = static_cast<uint>(CASE::INSTRUMENTAL);
        }
        else if (characteristics.find(u(' ')+case_t_prepositional) != std::string::npos)
        {
            wc.case_t = static_cast<uint>(CASE::PREPOSITIONAL);
        }
        else if (characteristics.find(u(' ')+case_t_callable) != std::string::npos)
        {
            wc.case_t = static_cast<uint>(CASE::CALLABLE);
        }
        else if (characteristics.find(u(' ')+case_t_countable) != std::string::npos)
        {
            wc.case_t = static_cast<uint>(CASE::COUNTABLE);
        }
        else if (characteristics.find(u(' ')+case_t_local) != std::string::npos)
        {
            wc.case_t = static_cast<uint>(CASE::LOCAL);
        }
        else if (characteristics.find(u(' ')+case_t_partitive) != std::string::npos)
        {
            wc.case_t = static_cast<uint>(CASE::PARTITIVE);
        }
    }

    // Если необходимо определеить время
    if (need_tense)
    {
        if (characteristics.find(u(' ')+tense_past) != std::string::npos)
        {
            wc.tense = static_cast<uint>(TENSE::PAST);
        }
        else if (characteristics.find(u(' ')+tense_present) != std::string::npos)
        {
            wc.tense = static_cast<uint>(TENSE::PRESENT);
        }
        else if (characteristics.find(u(' ')+tense_future) != std::string::npos)
        {
            wc.tense = static_cast<uint>(TENSE::FUTURE);
        }
        else if (characteristics.find(u(' ')+tense_infinitive) != std::string::npos)
        {
            wc.tense = static_cast<uint>(TENSE::INFINITIVE);
        }
    }

    // Если необходимо определить тип местоимения
    if (need_type && wc.part_of_speech == static_cast<uint>(PARTS_OF_SPEECH::PRONOUN))
    {
        if (characteristics.find(u(' ')+pronoun_adjectivelike) != std::string::npos)
        {
            wc.type = static_cast<uint>(PRONOUN_TYPE::ADJECTIVELIKE);
        }
        else if (characteristics.find(u(' ')+pronoun_adverblike) != std::string::npos)
        {
            wc.type = static_cast<uint>(PRONOUN_TYPE::ADVERBLIKE);
        }
        else if (characteristics.find(u(' ')+pronoun_nounlike) != std::string::npos)
        {
            wc.type = static_cast<uint>(PRONOUN_TYPE::NOUNLIKE);
        }
    }

    // Если необходимо определить тип наречия
    if (need_type && wc.part_of_speech == static_cast<uint>(PARTS_OF_SPEECH::ADVERB))
    {
        if (characteristics.find(u(' ')+adverb_condition_direction) != std::string::npos)
        {
            wc.type = static_cast<uint>(ADVERB_TYPE::CONDITION_DIRECTION);
        }
        else if (characteristics.find(u(' ')+adverb_condition_place) != std::string::npos)
        {
            wc.type = static_cast<uint>(ADVERB_TYPE::CONDITION_PLACE);
        }
        else if (characteristics.find(u(' ')+adverb_condition_purpose) != std::string::npos)
        {
            wc.type = static_cast<uint>(ADVERB_TYPE::CONDITION_PURPOSE);
        }
        else if (characteristics.find(u(' ')+adverb_condition_reason) != std::string::npos)
        {
            wc.type = static_cast<uint>(ADVERB_TYPE::CONDITION_REASON);
        }
        else if (characteristics.find(u(' ')+adverb_condition_time) != std::string::npos)
        {
            wc.type = static_cast<uint>(ADVERB_TYPE::CONDITION_TIME);
        }
        else if (characteristics.find(u(' ')+adverb_definition_degree) != std::string::npos)
        {
            wc.type = static_cast<uint>(ADVERB_TYPE::DEFINITION_DEGREE);
        }
        else if (characteristics.find(u(' ')+adverb_definition_quality) != std::string::npos)
        {
            wc.type = static_cast<uint>(ADVERB_TYPE::DEFINITION_QUALITY);
        }
        else if (characteristics.find(u(' ')+adverb_definition_way) != std::string::npos)
        {
            wc.type = static_cast<uint>(ADVERB_TYPE::DEFINITION_WAY);
        }
        else if (characteristics.find(u(' ')+adverb_question) != std::string::npos)
        {
            wc.type = static_cast<uint>(ADVERB_TYPE::QUESTION);
        }
    }

    // Если необходимо определеить тип числительного
    if (need_type && wc.part_of_speech == static_cast<uint>(PARTS_OF_SPEECH::PRONOUN))
    {
        if (characteristics.find(u(' ')+number_collective) != std::string::npos)
        {
            wc.type = static_cast<uint>(NUMBER_TYPE::COLLECTITIVE);
        }
        else if (characteristics.find(u(' ')+number_ordering) != std::string::npos)
        {
            wc.type = static_cast<uint>(NUMBER_TYPE::ORDERING);
        }
        else if (characteristics.find(u(' ')+number_quantitive) != std::string::npos)
        {
            wc.type = static_cast<uint>(NUMBER_TYPE::QUANTITIVE);
        }
        else if (characteristics.find(u(' ')+number_unstated) != std::string::npos)
        {
            wc.type = static_cast<uint>(NUMBER_TYPE::UNSTATED);
        }
    }

    // Если необходимо определеить род
    if (need_gender)
    {
        if (characteristics.find(u(' ')+gender_male) != std::string::npos)
        {
            wc.gender = static_cast<uint>(GENDER::MALE);
        }
        else if (characteristics.find(u(' ')+gender_female) != std::string::npos)
        {
            wc.gender = static_cast<uint>(GENDER::FEMALE);
        }
        else if (characteristics.find(u(' ')+gender_common) != std::string::npos)
        {
            wc.gender = static_cast<uint>(GENDER::COMMON);
        }
    }

    // Если необходимо определеить число
    if (need_quantity)
    {
        if (characteristics.find(u(' ')+quantity_singular) != std::string::npos)
        {
            wc.quantity = static_cast<uint>(QUANTITY::SINGULAR);
        }
        else if (characteristics.find(u(' ')+quantity_plural) != std::string::npos)
        {
            wc.quantity = static_cast<uint>(QUANTITY::PLURAL);
        }
    }

    // Если необходимо определить совершённость
    if (need_completeness)
    {

        if (characteristics.find(u(' ')+completeness_complete) != std::string::npos)
        {
            wc.completeness = static_cast<uint>(COMPLETNESS::COMPLETE);
        }
        else if (characteristics.find(u(' ')+completeness_incomplete) != std::string::npos)
        {
            wc.completeness = static_cast<uint>(COMPLETNESS::INCOMPLETE);
        }
        else if (characteristics.find(u(' ')+completeness_both) != std::string::npos)
        {
            wc.completeness = static_cast<uint>(COMPLETNESS::BOTH);
        }
    }

    // Если необходимо определеить переходность
    if (need_transitiveness)
    {
        if (characteristics.find(u(' ')+transitiveness_transitive) != std::string::npos)
        {
            wc.transitiveness = static_cast<uint>(TRANSITIVENESS::TRANSITIVE);
        }
        else if (characteristics.find(u(' ')+transitiveness_intransitive) != std::string::npos)
        {
            wc.transitiveness = static_cast<uint>(TRANSITIVENESS::INTRANSTITIVE);
        }
        else if (characteristics.find(u(' ')+transitiveness_both) != std::string::npos)
        {
            wc.transitiveness = static_cast<uint>(TRANSITIVENESS::BOTH);
        }
    }

    // Если необходимо определеить лицо
    if (need_personality)
    {
        if (characteristics.find(u(' ')+personality_first) != std::string::npos)
        {
            wc.personality = static_cast<uint>(PERSONALITY::FIRST);
        }
        else if (characteristics.find(u(' ')+personality_second) != std::string::npos)
        {
            wc.personality = static_cast<uint>(PERSONALITY::SECOND);
        }
        else if (characteristics.find(u(' ')+personality_third) != std::string::npos)
        {
            wc.personality = static_cast<uint>(PERSONALITY::THIRD);
        }
    }

    // Если необходимо определеить степень
    if (need_degree)
    {
        if (characteristics.find(u(' ')+degree_comparable) != std::string::npos)
        {
            wc.degree = static_cast<uint>(DEGREE::COMPARABLE);
        }
        else if (characteristics.find(u(' ')+degree_exessive) != std::string::npos)
        {
            wc.degree = static_cast<uint>(DEGREE::EXCESSIVE);
        }
    }

    // Если необходимо определеить возвратность
    if (need_selfness)
    {
        if (characteristics.find(u(' ')+selfness) != std::string::npos)
        {
            wc.selfness = static_cast<uint>(SELFNESS::SELF);
        }
    }

    // Если необходимо определеить наличие повелительного наклонения
    if (need_imperativeness)
    {
        if (characteristics.find(u(' ')+imperativeness) != std::string::npos)
        {
            wc.imperativeness = static_cast<uint>(IMPERATIVENESS::IMPERATIVE);
        }
    }

    // Если необходимо определеить наличие краткости
    if (need_shortness)
    {
        if (characteristics.find(u(' ')+shortness) != std::string::npos)
        {
            wc.shortness = static_cast<uint>(SHORTNESS::SHORT);
        }
    }

    // Если необходимо определеить наличие пассивного залога
    if (need_passive_voice)
    {
        if (characteristics.find(u(' ')+passive_voice) != std::string::npos)
        {
            wc.passive_voice = static_cast<uint>(PASSIVE_VOICE::PASSIVE);
        }
    }

    return static_cast<uint>(wc);
}

void ta::utils::parse_hagen_m_dictionary(const char* source, const char* dest)
{
    // Буфер для чтения строк из словаря на диске
    char line[128];
    // Входной поток, связанный со словарём на диске
    std::ifstream is;
    // Выходной поток, связанный с записанием упаковонного словаря
    std::ofstream os;
    // Выходной поток, связанный с записью словаря в оперативную память
    // В оперативной памяти слова хранятся подряд без разделителей
    uostringstream dictionary;
    // Массив объектов данных обо всех словоформах для хранения в оперативной памяти. Всего словоформ около 5 миллионов
    std::vector<word> words;

    log::d() << TAG << "Открытие исходного файла словаря, путь: " << source;
    is.open(source);

    if (!is.is_open())
    {
        log::e() << TAG << "не удалось открыть исходный файл словаря";
        return;
    }

    log::d() << TAG << "Создание выходного файла, путь: " << dest;
    os.open(dest);

    if (!os.is_open())
    {
        log::e() << TAG << "Не удалось открыть выходной файл для записи";
        return;
    }

    // ID лексемы. Это не ID из словаря М. Хагена, данный ID присваивается всем словоформама одного и того же слова
    uint id = 1;
    // Индекс словоформы в словаре, хранящемся в оперативной памяти
    uint index = 0;

    log::d() << TAG << "Начинается считывание слов из исходного файла";

    while(!is.eof())
    {
        is.getline(line, 128);

        // Статьи (слово со всеми его словоформами) разделены в словаре пустой строкой (содержит один пробел). Поэтому
        // переход к новой статье определяем выявлением CR на второй позиции в считанной строке
        bool is_new_lexeme = line[1] == '\r';

        // Новая лексема - увеличиваем ID
        if (is_new_lexeme)
        {
            ++id;
        }
        // Иначе обрабатываем очередную словоформу
        else
        {
            // Длина слова
            uint word_length = 0;
            // Поток характеристик
            uostringstream characteristics;
            // 32-битная интерпретация характеристик
            uint word_props = 0;
            // Флаг того, что встретился символ-разделитель "|", разделяющий слово от перечня его морфологических
            // характеристик
            bool separator_met = false;

            // Обработка строки
            for (char c : line)
            {
                // Если конец строки или мы встретили второй разделитель (отделяет характеристики слова от ударения и
                // проичх данных - нам нужны только харатеристики), то заканчиваем обработку строки
                if (c == 0 || (separator_met && c == '|'))
                {
                    // Если длина слова равна нулю, значит нам попался пробел, его в словарь не добавляем. Такое может
                    // быть в конце слваря
                    if (word_length == 0) break;
                    // Получаем числовую интерпретацию характеристик
                    word_props = parse_hagen_m_morphology(characteristics.str());
                    // Добавляем информацию о словоформе в массив мета-данных увеличиваем индекс для будущей словормы
                    uint code = ((id & 0xFFFFFFu)<<8) | (word_length&0xFFu);
                    words.push_back(word(index, code, word_props));
                    index += word_length;
                    break;
                }
                else
                {
                    // Попали на разделитель слова и характеристик
                    if (c == '|')
                    {
                        // После слова был записан лишний пробел
                        dictionary.seekp(-1, std::ios_base::cur);
                        --word_length;
                        separator_met = true;
                        continue;
                    }

                    // Разделителя ещё не было - считываем слово
                    if (!separator_met)
                    {
                        // Если это не первый пробел в строке, и не звёздочка (ей помечены неупотребляемые слова),
                        // записываем его
                        if ((c != ' ' || word_length > 0) && c != '*')
                        {
                            dictionary << static_cast<unsigned char>(c);
                            ++word_length;
                        }
                    }
                    // Если разделитель уже был, считываем характеристики
                    else
                    {
                        characteristics << static_cast<unsigned char>(c);
                    }
                }


            }
        }
    }

    log::d() << TAG << "Все слова считаны, начинается сортировка";

    word::dictionary = dictionary.str();
    // Сортиовка вектора слов по алфавиту
    std::sort(words.begin(), words.end());

    log::d() << TAG << "Сортировка закончена, запись двоичных данных в " << dest;

    // Запись данных в выходной файл

    // Текущий индекс содержит суммарное количество символов во всём словаре
    write_big_endian(index, os);
    // Запись общего числа словоформ
    write_big_endian(static_cast<uint>(words.size()), os);

    // Запись слов
    for (word w: words)
    {
        write_big_endian(w.props, os);
        write_big_endian(w.code, os);
        os << word::dictionary.substr(w.index, w.length()).c_str();
    }

    log::d() << TAG << "Данные записаны, закрытие потоков, связанных с файлами";

    os.close();
    is.close();
}

void ta::utils::write_big_endian(uint value, std::ostream& s)
{
    s << static_cast<unsigned char>((value & 0xFF000000u) >> 24)
      << static_cast<unsigned char>((value & 0x00FF0000u) >> 16)
      << static_cast<unsigned char>((value & 0x0000FF00u) >> 8)
      << static_cast<unsigned char>( value & 0x000000FFu);
}

uint ta::utils::read_big_endian(std::istream& s)
{
    uint result = 0;
    for (int i = 3; i >= 0; --i)
    {
        if (s.eof())
        {
            throw std::ios_base::failure("неверный формат данных, чтение невозможно");
        }
        result |= static_cast<uint>(s.get()) << (i*8);
    }

    return result;
}


