#ifndef TEXT_HPP
#define TEXT_HPP

#include "../env.hpp"
#include "word.hpp"

#include <map>
#include <vector>

#define OPTIMAL_DISTRIBUTION_SIZE 500

namespace ta
{
namespace grammar
{

class text
{
    // Текст в различных представлениях

    // Текст в виде массива предложений
    std::vector<const unsigned char*> sentences;
    // Текст в виде массива слов
    std::vector<word> words;
    // Текст в виде массива id лексем с соответствующими частотами
    std::map<uint, uint> lexems;
    // Вектор длин предложений в словах
    std::vector<uint> sentence_lengths;

    // Сравниваемые характеристики текста

    // (1-3) Текст в виде массива частот N-грамм, N = 2...4
    std::map<uint, uint> n_grams[3];
    // (4-6) Текст в виде массива частот буквенных N-грамм, N = 2...4
    std::map<uint, uint> n_grams_words[3];
    // (7)   Текст в виде распределения частей речи (часть речи; кол-во данных частей речи)
    std::map<uint, uint> part_of_speech_distribution;
    // (8)   Текст в виде распределения слов по длинам (длина; кол-во слов заданной длины)
    std::map<uint, uint> word_lengths_distribution;
    // (9)   Текст в виде распределения предложений по длинам слов (длина; кол-во предложений заданной длины)
    std::map<uint, uint> sentence_lengths_distribution;
    // (10)  Текст в виде распределения пар частей речи в начале предложения (пара; кол-во таких пар)
    std::map<uint, uint> part_of_speech_pairs_head_distribution;
    // (11)  Текст в виде распределения пар частей речи в конце предложения (пара; кол-во таких пар)
    std::map<uint, uint> part_of_speech_pairs_tail_distribution;
    // (12)  Текст в виде распределения троек частей речи в начале предложения (пара; кол-во таких пар)
    std::map<uint, uint> part_of_speech_trios_head_distribution;
    // (13)  Текст в виде распределения троек частей речи в конце предложения (пара; кол-во таких пар)
    std::map<uint, uint> part_of_speech_trios_tail_distribution;
    // (14)  Словарный профиль (частота слова; число слов с заданной частотой)
    std::map<uint, uint> word_profile;
    // (15)  Распределение слов по падежам
    std::map<uint, uint> case_distribution;
    // (16)  Относительная частота служебных частей речи
    double auxiliary_words_rate;
    // (17)  Коэффициент словарного состава
    double words_variery_rate;
    // (18)  Число причастий, деепричастий и глаголов с наличием возвратного суффикса "ся"
    double selfness_rate;

    // Дополнительные данные

    // Длина текста
    uint length;
    // Средняя длина предложения (в словах)
    double average_sentence_length;
    // Средняя длина слова в буквах
    double average_word_length;

    // Служебная информация

    // Автор
    char author[64] = {0};
    // Название
    char title[96] = {0};
    // Путь к файлу, откуда был считан текст
    const char* file_path;
    // Поток для печати информации о тексте
    std::ofstream info_stream;

    // Параметры анализа
    uint length_treshold;

public:

    /**
     * @brief text Считывает текст из файла, разбивая его на массив предложений
     * @param path Путь к файлу, содержащему текст
     * @param scan_mode Способ считывания текста из файла
     * @param settings Дополнительные настройки, используемые при разборе текста на предложения
     */
    text(const char* path, uint scan_mode = AUTHOR_DOT_TITLE, uint settings = SKIP_TITLES);

    ~text();

    /**
     * @brief analyze Осуществляет анализ текста
     * @param cut_distributions Еслти установлен как true - распределения урезаются до 1000 самых частых элементов
     * @return true в случае успешного анализа, false - иначе
     */
    bool analyze(uint length_treshold, bool cut_distributions = false);

    /**
     * @brief get_words Заполняет массив слов с помощью поиска каждого слова из текста по словарю
     */
    void get_words();

    /**
     * @brief get_n_grams
     * @param n
     * @param words_only
     */
    void get_n_grams(const uint n, bool words_only = true);

    /**
     * @brief get_part_of_speech_distribution Получает распределение частей речи
     */
    void get_part_of_speech_distribution();

    /**
     * @brief get_word_lengths_distribution Получает распределение слов по длинам
     */
    void get_word_lengths_distribution();

    /**
     * @brief get_word_lengths_distribution Получает распределение предложений по длинам
     */
    void get_sentence_lengths_distribution();

    /**
     * @brief get_part_of_speech_pairs_head_distribution Получает распределение пар частей речи в начале предложения
     */
    void get_part_of_speech_pairs_head_distribution();

    /**
     * @brief get_part_of_speech_pairs_head_distribution Получает распределение пар частей речи в конце предложения
     */
    void get_part_of_speech_pairs_tail_distribution();

    /**
     * @brief get_part_of_speech_pairs_head_distribution Получает распределение троек частей речи в начале предложения
     */
    void get_part_of_speech_trios_head_distribution();

    /**
     * @brief get_part_of_speech_pairs_head_distribution Получает распределение троек частей речи в конце предложения
     */
    void get_part_of_speech_trios_tail_distribution();

    /**
     * @brief get_word_profile Получает словарный профиль
     */
    void get_word_profile();

    /**
     * @brief get_case_distribution Получает распределение слов по падежам
     */
    void get_case_distribution();

    /**
     * @brief get_auxiliary_words_rate Получает относительную частоту служебных частей речи
     */
    void get_auxiliary_words_rate();

    /**
     * @brief get_words_variery_rate Получает коэффициент словарного состава
     */
    void get_words_variery_rate();

    /**
     * @brief get_selfness_rate Получает долю причастий, деепричастий и глаголов с наличием возвратного суффикса 'ся'
     */
    void get_selfness_rate();

    /**
     * @brief cut_distribution Обрезает распределение до OPTIMAL_DISTRIBUTION_SIZE самых высокочастотных элементов
     * @param d Распределение
     */
    void cut_distribution(std::map<uint, uint> &d);

    // Распечатка текста и информации о нём

    /**
     * @brief print_info_to Определяет файл для печати информации о тексте
     * @param path Путь к файлу
     */
    void print_info_to(const char* path);

    /**
     * @brief print_sentences Печатает список предложений, полученный при чтении текста из файла
     * @param delim_str Строка-разделитель, используемая для визуального разделения границ предложений
     */
    void print_sentences(const char* delim_str = nullptr);

    /**
     * @brief print_words Печатает найденные слова в сохранённый для печати поток
     */
    void print_words();

    /**
     * @brief print_n_grams Печатает n-граммы для заданного n
     * @param n от 2 до 4
     */
    void print_n_grams(uint n, bool words_only = true);

    /**
     * @brief print_distributions Печатает все распределения
     */
    void print_distributions();

    /**
     * @brief print_primitives Печатает примитивные характеристики
     */
    void print_primitives();

    /**
     * @brief print_all Печатает всю информацию о тексте
     * @param characteristics_only Если указано true, будут напечатаны только характеристики
     */
    void print_all(bool characteristics_only = false);

    /**
     * @brief write_distribution_as_binary Запись распределения в виде двоичных данных в массив
     * @param distribution Распределение
     * @param s Поток для записи
     */
    void write_distribution_as_binary(std::map<uint, uint> distribution, std::ofstream &s);

    /**
     * @brief write_primitive_as_binary Запись примитивной характеристики в двоичном формате
     * @param primitive Примитивная характеристика
     * @param s Поток для записи
     */
    void write_primitive_as_binary(double primitive, std::ofstream &s);

    /**
     * @brief save_as_binary Сохраняет характеристики текста в двоичном формате
     * @param to Файл для сохранения
     */
    void save_as_binary(const char* to);

    /**
     * @brief load_distribution Загружает распределение из бинарного файла характеристик
     * @param distribution Ссылка на данные для записи
     * @param data_size Размер считываемых данных
     * @param source Поток данных, откуда считываем
     */
    void load_distribution(std::map<uint, uint> &distribution, uint data_size, std::ifstream &source);

    /**
     * @brief load_primitive Загружает примитивную характеристику из бинарного файла характеристик
     * @param value Данные для записи
     * @param source Поток данных, откуда считываем
     */
    void load_primitive(double& value, std::ifstream &source);

    /**
     * @brief get_distribution_by_number Возвращает распределение по его номеру характеристики
     * @param number Номер характеристики
     * @return Распределение
     */
    std::map<uint,uint>& get_distribution_by_number(uint number);

    /**
     * @brief get_primitive_by_number Возвращает примитивную характеристику по номеру
     * @param number Номер характеристики
     * @return Значение характеристики
     */
    double get_primitive_by_number(uint number);

    /**
     * @brief get_author Возвращает имя автора текста
     * @return Автор текста
     */
    std::string get_author();

    inline void separator()
    {
        info_stream << "\n\n______________________________________________________________________________\n\n";
    }

    // Настойки при работе с текстами

    // Способы считать текст из файла
    enum scan_mode: uint
    {
        // Всё содержимое файла будет интерпретироваться как текст. Текст будет идентифицирован по имени файла
        PLAIN_DATA = 0,
        // В начале файла есть строка содержащая имя и фамилию автора, далее через точку следует название.
        // Со следующей строки идёт сам текст
        AUTHOR_DOT_TITLE = 1,
        // Файл содержит характеристики текста в бинарном формате
        BINARY_CHARACTERISTICS = 2
    };

    // Настройки для анализа текста
    enum analysis: uint
    {
        SKIP_TITLES = 0x1,                  // Пропускать заголовки
        SKIP_DIALOGS = 0x2,                 // Пропускать диалоги
    };

    static const uint MAX_TEXT_SIZE;
};

}
}

#endif // TEXT_HPP
