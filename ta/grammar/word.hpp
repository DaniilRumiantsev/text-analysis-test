#ifndef WORD_HPP
#define WORD_HPP

#include "grammar.hpp"
#include "../utils/windows1251.hpp"
#include "../utils/log.hpp"

namespace ta
{
namespace grammar
{

class word
{
public:

    // указатель на начало словаря в оперативной памяти. Все слова выгружаются в оперативную память подряд без пробелов
    // и других разделителей. Используется только в функции получения двоичного файла словаря из исходного текста
    static ustring&& dictionary;

    // Индекс слова в массиве-словаре
    uint index;
    // Код слова содержит 24-битный id лексемы и 8-битную запись длины слова
    uint code;
    // Морфологические свойства слова, упакованные в 32-разрядную битовую маску
    uint props;

    word(uint index, uint code, uint props);

    /**
     * @brief operator < Сравнивает два слова согласно их алфавитному порядку следования
     * @param other Правый операнд операции сравнения
     * @return true, если левый операнд должен следовать перед правым в алфавитном порядке, false - иначе
     */
    bool operator<(const word& other);

    /**
     * @brief length Выделяет длину слова (последний байт) из его кода
     * @return Длина слова
     */
    inline uint length() const
    {
        return code & 0xFFu;
    }

    /**
     * @brief id Возвращает id лексемы (первые три байта) из кода слова
     * @return id лексемы
     */
    inline uint id() const
    {
        return (code & 0xFFFFFF00u) >> 8;
    }

    /**
     * @brief part_of_speech Получает часть речи из характеристик (props) слова
     * @return Код части речи - первые 4 бита props
     */
    inline uint part_of_speech() const
    {
        return (props&0xF0000000u) >> 28;
    }

    /**
     * @brief is_auxiliary Определяет, является ли слово служебной частью речи
     * @return true если слово - служебная часть речи, false - иначе
     */
    inline bool is_auxiliary() const
    {
        return ta::grammar::is_auxiliary(part_of_speech());
    }

    /**
     * @brief case_t Получает падеж из характеристик (props) слова
     * @return Код падежа - старшие биты с номерами 5-8 в объекте props
     */
    inline uint case_t() const
    {
        return (props&0x0F000000u) >> 24;
    }

    /**
     * @brief is_self Проверяет, имеет ли слово возвратный суффикс 'ся' (4-й младший бит props)
     * @return true, если возвратный суффикс присутствует, false иначе
     */
    inline bool is_self() const
    {
        return (props&0x00000008u) >> 3;
    }
};
}
}

#endif // WORD_HPP
