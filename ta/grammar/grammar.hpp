#ifndef RUSSIAN_GRAMMAR_HPP
#define RUSSIAN_GRAMMAR_HPP

#include "../env.hpp"

namespace ta
{
namespace grammar
{

// Русский алфавит в кодировке Windows1251

enum class A: uint
{
    YO = 0xA9,                      // Ё
    yo = 0xB9,                      // ё

    A = 0xC0,                       // А
    B,                              // Б
    V,                              // В
    G,                              // Г
    D,                              // Д
    E,                              // Е
    ZH,                             // Ж
    Z,                              // З
    I,                              // И
    J,                              // Й
    K,                              // К
    L,                              // Л
    M,                              // М
    N,                              // Н
    O,                              // О
    P,                              // П
    R,                              // Р
    S,                              // С
    T,                              // Т
    U,                              // У
    F,                              // Ф
    H,                              // Х
    TS,                             // Ц
    CH,                             // Ч
    SH,                             // Ш
    SCH,                            // Щ
    HARD_SIGN,                      // Ъ
    Y,                              // Ы
    SOFT_SIGN,                      // Ь
    EH,                             // Э
    YU,                             // Ю
    YA,                             // Я

    a,                              // а
    b,                              // б
    v,                              // в
    g,                              // г
    d,                              // д
    e,                              // е
    zh,                             // ж
    z,                              // з
    i,                              // и
    j,                              // й
    k,                              // к
    l,                              // л
    m,                              // м
    n,                              // н
    o,                              // о
    p,                              // п
    r,                              // р
    s,                              // с
    t,                              // т
    u,                              // у
    f,                              // ф
    h,                              // х
    ts,                             // ц
    ch,                             // ч
    sh,                             // ш
    sch,                            // щ
    hard_sign,                      // ъ
    y,                              // ы
    soft_sign,                      // ь
    eh,                             // э
    yu,                             // ю
    ya,                             // я

};

// Части речи русского языка

enum class PARTS_OF_SPEECH: uint
{
    UNKNOWN,                        // Не указано
    INTRO,                          // Вводное слово
    VERB,                           // Глагол
    VERB_PARTICIPLE,                // Деепричастие
    INTERJECTION,                   // Междометие
    PRONOUN,                        // Местоимение
    ADVERB,                         // Наречие
    PREDICATE,                      // Предикат (категория состояния)
    PREPOSITION,                    // Предлог
    ADJECTIVE,                      // Прилагательное
    PARTICIPLE,                     // Причастие
    CONJUNCTION,                    // Союз
    NOUN,                           // Существительное
    PARTICLE,                       // Частица
    NUMBER,                         // Числительное
    COMPARATIVE                     // Компаратив
};

// Свойства частей речи

// Падежи (существительные, предлоги, прилагательные и т.д.)

enum class CASE: uint
{
    UNKNOWN,                        // Не указано
    NOMINATIVE,                     // Именительный
    GENETIVE,                       // Родительный
    DATIVE,                         // Дательный
    ACUSSATIVE,                     // Винительный
    INSTRUMENTAL,                   // Творительный
    PREPOSITIONAL,                  // Предложный
    // Особые падежи
    PARTITIVE,                      // Партитивный
    COUNTABLE,                      // Счётный
    LOCAL,                          // Местный
    CALLABLE                        // Звательный

};

// Род (существительные, прилагательные, глаголы, причастия и т.д.)

enum class GENDER: uint
{
    UNKNOWN,                        // Не указано
    MALE,                           // Мужской
    FEMALE,                         // Женский
    COMMON                          // Общий
};

// Число (существительные, прилагательные, глаголы, причастия и т.д.)

enum class QUANTITY: uint
{
    UNKNOWN,                        // Не указано
    SINGULAR,                       // Единственное
    PLURAL                          // Множественное
};

// Совершённость/несовершённость (глаголы, деепричастия, причастия)

enum class COMPLETNESS: uint
{
    UNKNOWN,                        // Не указано
    COMPLETE,                       // Совершённый
    INCOMPLETE,                     // Несовершённый
    BOTH                            // 2 вида
};

// Переходность (глаголы, деепричастия, причастия)

enum class TRANSITIVENESS: uint
{
    UNKNOWN,                        // Не указано
    TRANSITIVE,                     // Переходный
    INTRANSTITIVE,                  // Непереходный
    BOTH                            // 2 вида
};

// Возвратность (глаголы, деепричастия, причастия)

enum class SELFNESS: uint
{
    UNKNOWN,                        // Не указано
    SELF                            // Возвратный
};

// Время (глаголы, деепричастия, причастия)

enum class TENSE: uint
{
    UNKNOWN,                        // Не указано
    PAST,                           // Пршедшее
    PRESENT,                        // Настоящее
    FUTURE,                         // Будущее
    INFINITIVE                      // Инфинитив
};

// Лицо (глаголы)

enum class PERSONALITY: uint
{
    UNKNOWN,                        // Не указано
    FIRST,                          // Первое
    SECOND,                         // Второе
    THIRD,                          // Третье
};

// Наклонение (глаголы)

enum class IMPERATIVENESS: uint
{
    UNKNOWN,                        // Не указано
    IMPERATIVE                      // Повелительное
};

// Тип местоимения

enum class PRONOUN_TYPE: uint
{
    UNKNOWN,                        // Не указано
    ADVERBLIKE,                     // Наречное
    NOUNLIKE,                       // Существительное
    ADJECTIVELIKE                   // Прилагательное
};

// Тип наречия

enum class ADVERB_TYPE: uint
{
    UNKNOWN,                        // Не указано
    QUESTION,                       // Вопросительное
    CONDITION_TIME,                 // Обстоятельства времени
    CONDITION_PLACE,                // Обстоятельства места
    CONDITION_DIRECTION,            // Обстоятельсва напрвоения
    CONDITION_REASON,               // Обстоятельства причины
    CONDITION_PURPOSE,              // Обстоятельства цели
    DEFINITION_QUALITY,             // Определителения качества
    DEFINITION_WAY,                 // Определения способа
    DEFINITION_DEGREE,              // Определения степени
};

// Степень (прилагательные и наречия)

enum class DEGREE: uint
{
    UNKNOWN,                        // Не указано
    EXCESSIVE,                      // Превосходная
    COMPARABLE                      // Сравнительная
};

// Краткость (прилагательные и причастия)

enum class SHORTNESS: uint
{
    UNKNOWN,                        // Не указано
    SHORT                           // Краткое
};

// Наличие страдательного залога (причастия)

enum class PASSIVE_VOICE: uint
{
    UNKNOWN,                        // Не указано
    PASSIVE                         // Страдательный
};

// Одушевлённотсь (существительные, прилагательные, причастия, числительные)

enum class LIVENESS: uint
{
    UNKNOWN,                        // Не указано
    ANIMATE,                        // Одушевлёенный
    INANIMATE                       // Неодушевлённый
};

// Изменяемость (прилагательные)

enum class MUTABILITY: uint
{
    UNKNOWN,                        // Не указано
    IMMUTABLE                       // Неизменяемое
};

// Тип числительных

enum class NUMBER_TYPE: uint
{
    UNKNOWN,                        // Не указано
    QUANTITIVE,                     // Количественное
    COLLECTITIVE,                   // Собирательное
    ORDERING,                       // Порядковое
    UNSTATED                        // Неопредлённое
};

// Морфологические характеристики слова

// Морфологические характеристики частей речи (32-битное представление)
// Для каждой части речи заполняются только некоторые поля, остальные остаются пустыми. Для экономии памяти можно
// было бы для каждой части речи делать свой набор характеристик, но выравнивае в памяти всё равно добавит лишние 2
// байта, поэтому смысл в такой экономии отпадает

struct word_characteristics
{
    uint part_of_speech: 4;     // Часть речи
    uint case_t: 4;             // Падеж
    uint tense: 4;              // Время
    uint type: 4;               // Тип
    uint gender: 2;             // Род
    uint quantity: 2;           // Число
    uint completeness: 2;       // Совершённость/несовершённость
    uint transitiveness: 2;     // Переходность
    uint personality: 2;        // Лицо
    uint degree: 2;             // Степень
    uint selfness: 1;           // Наличие возвратного суффикса "ся"
    uint imperativeness: 1;     // Налииче повелительного наклонения
    uint shortness: 1;          // Краткое или нет
    uint passive_voice: 1;      // Страдательное или нет

    // При создании объекта изначально везде нули
    word_characteristics()
        : part_of_speech(0),
          case_t(0),
          tense(0),
          type(0),
          gender(0),
          quantity(0),
          completeness(0),
          transitiveness(0),
          personality(0),
          degree(0),
          selfness(0),
          imperativeness(0),
          shortness(0),
          passive_voice(0)
    {

    }

    // Приведение к типу uint
    explicit operator uint()
    {
        return  ((part_of_speech & 0x0Fu) << 28) |
                ((case_t         & 0x0Fu) << 24) |
                ((tense          & 0x0Fu) << 20) |
                ((type           & 0x0Fu) << 16) |
                ((gender         & 0x03u) << 14) |
                ((quantity       & 0x03u) << 12) |
                ((completeness   & 0x03u) << 10) |
                ((transitiveness & 0x03u) <<  8) |
                ((personality    & 0x03u) <<  6) |
                ((degree         & 0x03u) <<  4) |
                ((selfness       & 0x01u) <<  3) |
                ((imperativeness & 0x01u) <<  2) |
                ((shortness      & 0x01u) <<  1) |
                (passive_voice  & 0x01u);
    }
};

// Код слова, состоящий из идетификатора, уникольного для группы словоформ, относящейся к одной лексемме, и 8-битной
// длины слова
struct word_code
{
    uint id: 24;
    uint length: 8;
};

/**
 * @brief u Быстрая функция преобразования буквы из перечислимого типа в
 * unsigned char
 * @param value Значение из перечесления ALPHABET
 * @return Буква как тип unsigned char
 */
inline unsigned char u(A value)
{
    return static_cast<unsigned char>(value);
}

/**
 * @brief u Быстрая функция преобразования буквы из char в unsigned char
 * @param value Значение из перечесления ALPHABET
 * @return Буква как тип unsigned char
 */
inline unsigned char u(char value)
{
    return static_cast<unsigned char>(value);
}

/**
 * @brief part_of_speech Возвращает название части речи по её коду
 * @param code Код части речи
 * @return Название части речи
 */
ustring part_of_speech(uint code);

/**
 * @brief case_name Возвращает название падежа по его коду
 * @param code Код падежа
 * @return Название падежа
 */
ustring case_name(uint code);

/**
 * @brief is_auxiliary Проверяет, является ли часть речи служебной
 * @param code Код части речи
 * @return true, если часть речи служебная, false - иначе
 */
inline bool is_auxiliary(uint code)
{
    return  static_cast<PARTS_OF_SPEECH>(code) == PARTS_OF_SPEECH::PARTICLE ||
            static_cast<PARTS_OF_SPEECH>(code) == PARTS_OF_SPEECH::CONJUNCTION ||
            static_cast<PARTS_OF_SPEECH>(code) == PARTS_OF_SPEECH::PREPOSITION ||
            static_cast<PARTS_OF_SPEECH>(code) == PARTS_OF_SPEECH::INTERJECTION;
}

} // grammar
} // ta

#endif // RUSSIAN_GRAMMAR_HPP
