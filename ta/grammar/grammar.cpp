#include "grammar.hpp"

using namespace ta;
using namespace ta::grammar;

ustring ta::grammar::part_of_speech(uint code)
{
    PARTS_OF_SPEECH p_code = static_cast<PARTS_OF_SPEECH>(code);

    switch (p_code)
    {
        case PARTS_OF_SPEECH::UNKNOWN:
            return ustring{u(A::N), u(A::e), ' ', u(A::o), u(A::p), u(A::r), u(A::e), u(A::d), '.'};
        case PARTS_OF_SPEECH::INTRO:
            return ustring{u(A::V), u(A::v), u(A::o), u(A::d), '.', ' ', u(A::s), u(A::l), '.'};
        case PARTS_OF_SPEECH::VERB:
            return ustring{u(A::G), u(A::l), u(A::a), u(A::g), u(A::o), u(A::l)};
        case PARTS_OF_SPEECH::VERB_PARTICIPLE:
            return ustring{u(A::D), u(A::e), u(A::e), u(A::p), u(A::r), u(A::i), u(A::ch), '.'};
        case PARTS_OF_SPEECH::INTERJECTION:
            return ustring{u(A::M), u(A::e), u(A::zh), u(A::d), u(A::o), u(A::m), '.'};
        case PARTS_OF_SPEECH::PRONOUN:
            return ustring{u(A::M), u(A::e), u(A::s), u(A::t), u(A::o), u(A::i), u(A::m), '.'};
        case PARTS_OF_SPEECH::ADVERB:
            return ustring{u(A::N), u(A::a), u(A::r), u(A::e), u(A::ch), u(A::i), u(A::e)};
        case PARTS_OF_SPEECH::PREDICATE:
            return ustring{u(A::K), u(A::a), u(A::t), '.', ' ', u(A::s), u(A::o), u(A::s), u(A::t), '.'};
        case PARTS_OF_SPEECH::PREPOSITION:
            return ustring{u(A::P), u(A::r), u(A::e), u(A::d), u(A::l), u(A::o), u(A::g)};
        case PARTS_OF_SPEECH::ADJECTIVE:
            return ustring{u(A::P), u(A::r), u(A::i), u(A::l), u(A::a), u(A::g), '.'};
        case PARTS_OF_SPEECH::PARTICIPLE:
            return ustring{u(A::P), u(A::r), u(A::i), u(A::ch), u(A::a), u(A::s), u(A::t), u(A::i), u(A::e)};
        case PARTS_OF_SPEECH::CONJUNCTION:
            return ustring{u(A::S), u(A::o), u(A::yu), u(A::z)};
        case PARTS_OF_SPEECH::NOUN:
            return ustring{u(A::S), u(A::u), u(A::sch), '.'};
        case PARTS_OF_SPEECH::PARTICLE:
            return ustring{u(A::CH), u(A::a), u(A::s), u(A::t), u(A::i), u(A::ts), u(A::a)};
        case PARTS_OF_SPEECH::NUMBER:
            return ustring{u(A::CH), u(A::i), u(A::s), u(A::l), '.'};
        case PARTS_OF_SPEECH::COMPARATIVE:
            return ustring{u(A::K), u(A::o), u(A::m), u(A::p), u(A::a), u(A::r), u(A::a), u(A::t), '.'};
    }
}

ustring ta::grammar::case_name(uint code)
{
    CASE c_code = static_cast<CASE>(code);

    switch (c_code)
    {
        case CASE::UNKNOWN:
            return ustring{u(A::N), u(A::e), ' ', u(A::o), u(A::p), u(A::r), u(A::e), u(A::d), '.'};
        case CASE::NOMINATIVE:
            return ustring{u(A::I), u(A::m), '.'};
        case CASE::GENETIVE:
            return ustring{u(A::R), u(A::o), u(A::d), '.'};
        case CASE::DATIVE:
            return ustring{u(A::D), u(A::a), u(A::t), '.'};
        case CASE::ACUSSATIVE:
            return ustring{u(A::V), u(A::i), u(A::n), '.'};
        case CASE::INSTRUMENTAL:
            return ustring{u(A::T), u(A::v), u(A::o), u(A::r), '.'};
        case CASE::PREPOSITIONAL:
            return ustring{u(A::P), u(A::r), u(A::e), u(A::d), '.'};
        case CASE::PARTITIVE:
            return ustring{u(A::P), u(A::a), u(A::r), u(A::t), '.'};
        case CASE::COUNTABLE:
            return ustring{u(A::S), u(A::ch), u(A::yo), u(A::t), '.'};
        case CASE::LOCAL:
            return ustring{u(A::M), u(A::e), u(A::s), u(A::t), '.'};
        case CASE::CALLABLE:
            return ustring{u(A::Z), u(A::v), u(A::a), u(A::t), '.'};


    }
}
