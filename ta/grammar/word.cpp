#include "word.hpp"

using namespace ta;
using namespace ta::grammar;

ustring&& word::dictionary = ustring{};


word::word(uint index, uint code, uint props): index(index), code(code), props(props)
{

}

bool word::operator<(const word& other)
{
    bool this_shorter = length() < other.length();
    uint iters = this_shorter ? length() : other.length();

    for (uint i = 0; i < iters; ++i)
    {
        int result = ta::utils::windows1251::compare_chars(dictionary[index+i], dictionary[other.index+i]);
        // Если буквы на i-той позиции равны, сравниваем дальше
        if (!result) continue;
        return result < 0;
    }

    return this_shorter;
}
