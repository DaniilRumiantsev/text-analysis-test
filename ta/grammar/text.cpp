#ifndef TEXT_CPP
#define TEXT_CPP

#include "grammar.hpp"
#include "text.hpp"
#include "../utils/windows1251.hpp"
#include "../tools/dictionary.hpp"

using namespace ta::grammar;
using namespace ta::tools;
using namespace ta::utils;
using namespace ta::utils::windows1251;

const uint text::MAX_TEXT_SIZE = 1 << 24;

text::text(const char* path, uint scan_mode, uint settings)
    : length(0),
      average_sentence_length(0),
      average_word_length(0),
      author(""),
      title(""),
      file_path(path)
{
    std::ifstream s;
    s.open(path, std::ifstream::in);
    if (!s.is_open() || s.bad())
    {
        throw std::ios_base::failure(std::string("Невозможно открыть файл ")+path);
    }

    log::d() << TAG << "Считывание текста из " << path;

    // Если файл содержит бинарные характеристики, считываем их

    if (scan_mode == BINARY_CHARACTERISTICS)
    {
        // Считываем все 18 характеристик
        try
        {
            for (uint i = 1; i <= 18; ++i)
            {
                uint data_size = read_big_endian(s);

                switch (i)
                {
                    // Распределение N-грамм
                    case 1:
                    case 2:
                    case 3:
                         load_distribution(n_grams[i-1], data_size, s);
                    break;

                    // Распределение буквенных N-грамм
                    case 4:
                    case 5:
                    case 6:
                         load_distribution(n_grams_words[i-4], data_size, s);
                    break;

                    // Распределение частей речи
                    case 7:
                        load_distribution(part_of_speech_distribution, data_size, s);
                    break;

                    // Распределение длин слов
                    case 8:
                        load_distribution(word_lengths_distribution, data_size, s);
                    break;

                    // Распределение длин предложений
                    case 9:
                        load_distribution(sentence_lengths_distribution, data_size, s);
                    break;

                    // Распределение пар частей речи в начале предложения
                    case 10:
                        load_distribution(part_of_speech_pairs_head_distribution, data_size, s);
                    break;

                    // Распределение пар частей речи в конце предложения
                    case 11:
                        load_distribution(part_of_speech_pairs_tail_distribution, data_size, s);
                    break;

                    // Распределение троек частей речи в начале предложения
                    case 12:
                        load_distribution(part_of_speech_trios_head_distribution, data_size, s);
                    break;

                    // Распределение троек частей речи в конце предложения
                    case 13:
                        load_distribution(part_of_speech_trios_tail_distribution, data_size, s);
                    break;

                    // Словарный профиль
                    case 14:
                        load_distribution(word_profile, data_size, s);
                    break;

                    // Распределение падежей
                    case 15:
                        load_distribution(case_distribution, data_size, s);
                    break;

                    // Доля служебных частей речи
                    case 16:
                         load_primitive(auxiliary_words_rate, s);
                    break;

                    // КСС
                    case 17:
                        load_primitive(words_variery_rate, s);
                    break;

                    // Доля слов с возвратным суффиксом
                    case 18:
                         load_primitive(selfness_rate, s);
                    break;
                }
            }
        }
        catch (std::exception& e)
        {
            log::e() << TAG << "Считывание двоичных характеристик прошло неудачно. " << e.what();
        }

        return;
    }

    // Иначе, считываем текст из источника

    // Считываем автора и название текста из первой строки
    if (scan_mode & text::scan_mode::AUTHOR_DOT_TITLE)
    {
        // Считываем автора
        s.getline(author, 64, '.');
        // Пропускаем пробел
        s.ignore();
        // Считываем название
        s.getline(title, 96);
    }

    log::d() << TAG << "Автор и заголовок считаны";

    // Считываем текст построчно (до встречи LF). В данном случае строка - это абзац
    while (!s.eof())
    {
        std::string paragraph;
        std::getline(s, paragraph);

        // удаляем все пробельные символы в начале и конце строки
        paragraph.erase(0, paragraph.find_first_not_of(" \r\n\t\v\f"));
        paragraph.erase(paragraph.find_last_not_of(" \r\n\t\v\f")+1);

        // Если строка осталась (или и так была) пустая, пропускаем её
        // Если в строке вдруг и осталось 1 или 2 символа, это едва ли что-то осмысленное
        if (paragraph.size() < 3) continue;

        // Если включён режим пропуска заголовков, а строка не заканчивается на точку, троеточие, вопросительный,
        // восклицательный знаки, двоеточие, точку с запятой или многоточие, значит, это скорее всего заголовок
        // Однако и заголовки и предложения с прямой речью могут оканчиваться на кавычки. Но в предложении с прямой
        // речью пред кавычкой наверняка будет точка, многоточие, вопросительный или восклицательный знак
        if ((settings & SKIP_TITLES) &&
                !(ENCODING[static_cast<unsigned char>(paragraph.back())] & CHAR_TYPES::NON_TITLE_ENDING))
        {
            if (!((ENCODING[static_cast<unsigned char>(paragraph.back())] &
                    (CHAR_TYPES::CLOSING_DOUBLE_ANGLE_QUOTATION_MARK|CHAR_TYPES::QUOTATION_MARK)) &&
                    (ENCODING[static_cast<unsigned char>(paragraph[paragraph.length()-2])] &
                      (CHAR_TYPES::PERIOD |
                       CHAR_TYPES::EXCLAMATION_POINT |
                       CHAR_TYPES::QUESTION_MARK |
                       CHAR_TYPES::ELLIPSIS))))
            continue;
        }

        // Если включён режим пропуска диалогов, а строка начинается с тире или дефиса, за которым следует пробел,
        // то это скорее всего прямая речь, и такую строку можно пропустить
        if ((settings & SKIP_DIALOGS) &&
            (ENCODING[static_cast<unsigned char>(paragraph[0])] & (CHAR_TYPES::DASH|CHAR_TYPES::HYPHEN)) &&
            (paragraph[1] == ' '))
        {
            continue;
        }

        // Каждый абзац разбиваем на предложения. Для этого нужны флаги состояний на момент прочтения каждого
        // последующего символа
        bool is_sentence_ending = false;
        bool is_space_written = false;
        bool is_first_char_of_sentence = true;
        bool is_three_dots = false;
        bool is_sub_opened = false;
        bool is_exclamation_mark_after_question_mark = false;

        // Предложение
        uostringstream sentence;

        for (uint i = 0; i < paragraph.length(); ++i)
        {
            // Получаем текущий символ
            unsigned char c = static_cast<unsigned char>(paragraph[i]);

            // Если это последний символ в абзаце, это конец предложения
            if (i == paragraph.length()-1)
            {
                is_sentence_ending = true;
            }

            // Если наткнулись на пробельный символ
            if (ENCODING[c] & CHAR_TYPES::WHITE_CHAR)
            {
                // Если предыдущий символ не был пробелом и мы не в самом начале предложения, то записываем этот пробел
                if (!is_space_written && !is_first_char_of_sentence)
                {
                    sentence << c;
                    is_space_written = true;
                }
                // Переходим к следующему символу
                continue;
            }
            // Если наткнулись на открывающую кавычку или скобку
            else if (ENCODING[c] & (CHAR_TYPES::OPENING_DOUBLE_ANGLE_QUOTATION_MARK | CHAR_TYPES::OPENING_PARENTHESES))
            {
                is_sub_opened = true;
                sentence << c;
            }
            // Если наткнулись на закрывающую кавычку или скобку
            else if (ENCODING[c] & (CHAR_TYPES::CLOSING_DOUBLE_ANGLE_QUOTATION_MARK | CHAR_TYPES::CLOSING_PARENTHESES))
            {
                is_sub_opened = false;
                sentence << c;
            }
            // Если наткнулись на обычную кавычку
            else if (ENCODING[c] & (CHAR_TYPES::QUOTATION_MARK))
            {
                is_sub_opened = !is_sub_opened;
                sentence << c;
            }
            // Если мы имеем что-то похожее на конец предложения
            else if (ENCODING[c] & CHAR_TYPES::SENTENCE_ENDING)
            {

                // Если мы внутри открытых скобок или кавычек, и проверкой выше не было установлено, что это последний
                // символ в абзаце, то содержимое кавычек или скобок отнесём к текущему предложению
                if (!is_sentence_ending && is_sub_opened)
                {
                    is_sentence_ending = false;
                }
                // Если это ? или !, то скорее всего это конец предложения, если после них не следует дефиса или тире,
                // обозначающих начало слов автора после прямой речи
                else if (c == '?' || c == '!')
                {
                    // Если после вопросительного знака идёт восклицательный, запоминаем это и перепрыгиваем через
                    // восклицательный знак
                    if (c == '?' && i < paragraph.length()-1 && paragraph[i+1] == '!')
                    {
                        ++i;
                        is_exclamation_mark_after_question_mark = true;
                    }
                    uint next = i;
                    // Пропуск всех пробелов
                    while(++next < paragraph.length() &&
                          (ENCODING[static_cast<unsigned char>(paragraph[next])] & CHAR_TYPES::WHITE_CHAR));
                    // Проверка того, что далее не идёт дефис или тире
                    if ((next >= paragraph.length()) |
                        (next < paragraph.length() &&
                        !(ENCODING[static_cast<unsigned char>(paragraph[next])]&(CHAR_TYPES::HYPHEN|CHAR_TYPES::DASH))))
                    {
                        is_sentence_ending = true;
                    }
                }
                // Если это точка или многоточие, а далее идёт слово с большой буквы, то скорее всего, это тоже конец
                // предложения
                else if (c == '.' || c == 0x85)
                {
                    // Если мы видим многоточие в самом начале абзаца, то это не конец предложения
                    if (i == 0)
                    {
                        if ((c == 0x85) || (c == '.' && paragraph[1] == '.' && paragraph[2] == '.'))
                        {
                            is_sentence_ending = false;
                            if (c != 0x85)
                            {
                                is_three_dots = true;
                                i += 2;
                            }
                        }
                    }
                    else
                    {
                        // Если это точка, проверим, что дальше нет ещё двух точек. Иногда троеточие может записываться
                        // тремя символами
                        if (c == '.' && i+2 < paragraph.length() && paragraph[i+1] == '.' && paragraph[i+2] == '.')
                        {
                            // Следующие две точки записывать не будем
                            i += 2;
                            // Запоминаем, что это троеточие
                            is_three_dots = true;
                            // Если в итоге мы оказались в конце абзаца, это конец предложения
                            if (i == paragraph.length()-1)
                            {
                                is_sentence_ending = true;
                            }
                        }
                        // Далее ищем большую букву, пропуская пробелы. Ещё новое предложение может начаться с цифры
                        uint next = i;
                        // Пропуск всех пробелов
                        while(++next < paragraph.length() &&
                              (ENCODING[static_cast<unsigned char>(paragraph[next])] & CHAR_TYPES::WHITE_CHAR));
                        // если не достигли конца абзаца
                        if (next < paragraph.length())
                        {
                            // Если это большая буква или цифра - конец предложения
                            if (ENCODING[static_cast<unsigned char>(paragraph[next])]&
                             ( CHAR_TYPES::RUSSIAN_UPPERCASE_LETTER |
                               CHAR_TYPES::ENGLISH_UPPERCASE_LETTER |
                               CHAR_TYPES::DIGIT))
                            {
                                is_sentence_ending = true;
                            }
                        }
                    }
                }

                // Записываем считанный символ или троеточие вместо трёх точек
                sentence << (is_three_dots ? static_cast<unsigned char>(0x85) : c);
                if (is_exclamation_mark_after_question_mark) sentence << static_cast<unsigned char>('!');


            }
            // Иначе просто сохраняем текущий символ в предложение
            else
            {
                sentence << c;
                is_space_written = false;
                is_first_char_of_sentence = false;
                is_three_dots = false;
                is_exclamation_mark_after_question_mark = false;
            }

            // Если это в итоге конец предложения, сохраняем его и готовимся записывать следующее
            if (is_sentence_ending)
            {
                // Устанавливаем флаги в положение, соответствующее началу предложения
                is_sentence_ending = false;
                is_space_written = false;
                is_first_char_of_sentence = true;
                is_three_dots = false;
                is_sub_opened = false;
                is_exclamation_mark_after_question_mark = false;
                // увеличиваем длину текста на длину предложения
                length += sentence.tellp();
                // Сохраняем полученное предложение
                uint sentence_ntbs_length = static_cast<uint>(sentence.str().length())+1;
                unsigned char* sp = new unsigned char[sentence_ntbs_length];
                std::memcpy(sp, sentence.str().c_str(), sentence_ntbs_length);
                sentences.push_back(sp);
                // Освобождаем буфер для нового предложения
                sentence.str(std::basic_string<unsigned char>());
            }
        }
    }

    s.close();
}

text::~text()
{
    for (const unsigned char* sentence : sentences)
    {
        delete [] sentence;
    }

    if (info_stream.is_open())
    {
        info_stream.close();
    }
}

bool text::analyze(uint length_treshold, bool cut_distributions)
{
    // Если длина текста меньше пороговой, анализ не состоится
    if (length < length_treshold) return false;

    this->length_treshold = length_treshold;

    std::thread t0(&text::get_words, this);
    std::thread t1(&text::get_n_grams, this, 2, false);
    std::thread t2(&text::get_n_grams, this, 3, false);
    std::thread t3(&text::get_n_grams, this, 4, false);
    std::thread t4(&text::get_n_grams, this, 2, true);
    std::thread t5(&text::get_n_grams, this, 3, true);
    std::thread t6(&text::get_n_grams, this, 4, true);

    t0.join();
    t1.join();
    t2.join();
    t3.join();
    t4.join();
    t5.join();
    t6.join();

    std::thread t7(&text::get_part_of_speech_distribution, this);
    std::thread t8(&text::get_word_lengths_distribution, this);
    std::thread t9(&text::get_sentence_lengths_distribution, this);
    std::thread t10(&text::get_part_of_speech_pairs_head_distribution, this);
    std::thread t11(&text::get_part_of_speech_pairs_tail_distribution, this);
    std::thread t12(&text::get_part_of_speech_trios_head_distribution, this);
    std::thread t13(&text::get_part_of_speech_trios_tail_distribution, this);
    std::thread t14(&text::get_word_profile, this);
    std::thread t15(&text::get_case_distribution, this);
    std::thread t16(&text::get_auxiliary_words_rate, this);
    std::thread t17(&text::get_words_variery_rate, this);
    std::thread t18(&text::get_selfness_rate, this);

    t7.join();
    t8.join();
    t9.join();
    t10.join();
    t11.join();
    t12.join();
    t13.join();
    t14.join();
    t15.join();
    t16.join();
    t17.join();
    t18.join();

    if (cut_distributions)
    {
        cut_distribution(n_grams[0]);
        cut_distribution(n_grams[1]);
        cut_distribution(n_grams[2]);
        cut_distribution(n_grams_words[0]);
        cut_distribution(n_grams_words[1]);
        cut_distribution(n_grams_words[2]);

        cut_distribution(part_of_speech_pairs_head_distribution);
        cut_distribution(part_of_speech_pairs_tail_distribution);
        cut_distribution(part_of_speech_trios_head_distribution);
        cut_distribution(part_of_speech_trios_tail_distribution);

        cut_distribution(word_profile);
    }

    return true;
}

void text::get_words()
{
    log::d() << TAG << "Начинается процедура разбития текста на слова";

    // Получаем экземпляр словаря
    dictionary& d = dictionary::get();

    // Из максимум скольких простых слов может состоять состаное
    constexpr uint max_compound_word = 4;

    // Накопленная сумма длин предложений в словах
    uint total_length = 0;
    // Накопленная сумма длин слов в символах
    uint total_word_length = 0;
    // Счётчик считанных символов
    uint chars_count = 0;

    for (const unsigned char* sentence: sentences)
    {
        // Запоминаем, сколько слов было до этого
        uint words_total = static_cast<uint>(words.size());
        // Вектор, обозначающий границы последних нескольких встретившихся слов, разделённых пробелами
        std::vector<std::pair<uint,uint>> last_words;
        // Флаг того, что в границах слова (предыдущий символ был буквой или дефисом, следующим за буквой)
        bool inside_word = false;
        // Вектор накопленных слов должен быть очищен (либо достигли max_compound_word слов, либо наткнулись на знак
        // препинания, который свидетельствует о том, что слово точно закончилось
        bool saved_words_need_erase = false;

        for (uint i = 0; unsigned char c = sentence[i]; ++i)
        {
            // Увеличиваем счётчик считанных символов
            ++chars_count;
            // Останавливаемся при достижении пороговой длины для анализа
            if (chars_count >= length_treshold) break;

            // Если втречаем букву или дефис
            if (ENCODING[c] & CHAR_TYPES::WORD_CHAR)
            {
                if (!inside_word)
                {
                    // Если наткнулись на дефис и до этого была не буква, похоже, что это тире
                    if (c == '-')
                    {
                        saved_words_need_erase = true;
                    }
                    // А если это буква, помечаем, что мы в границах слова и запоминаем позицию начала слова
                    else
                    {
                        inside_word = true;
                        last_words.push_back({i,0});
                    }
                }
            }
            // Если мы наткнулись на пробел, не факт, что это конец слова. В словаре есть составные слова, состоящие из
            // 2-4 простых слов (несмотря на, по умолчанию, не в ладах и др.)
            // Сначала мы будет проверять в словаре наличие комбинации из четырёх слов, если такой нет, то из трёх. Если
            // и её нет, то из двух, в конце концов из одного слова.
            else if (ENCODING[c] & CHAR_TYPES::WHITE_CHAR)
            {
                // Если до этого мы находились в границах простого слова
                if (inside_word)
                {
                    // Сохраняем конец слова в вектор сохранённых слов
                    last_words.back().second = i;
                    // Если в векторе накопилось максимально возможное число простых слов, помечаем его, как требуемый
                    // для очистки
                    if (last_words.size() == max_compound_word)
                    {
                        saved_words_need_erase = true;
                    }
                }

                inside_word = false;
            }
            // Если это не дефис, не буква, не пробел - это точно не слово. Сбрасываем переменные состояния
            else
            {
                if (inside_word)
                {
                    // Сохраняем конец слова в вектор сохранённых слов
                    last_words.back().second = i;
                }
                inside_word = false;
                saved_words_need_erase = true;
            }

            // Очищаем вектор накопленных слов при необходимости
            while (saved_words_need_erase && last_words.size() > 0)
            {
                uint words_count = static_cast<uint>(last_words.size());
                while (true)
                {
                    uint id = d.find(sentence+last_words.front().first, sentence+last_words[words_count-1].second);
                    if (id == dictionary::absent)
                    {
                        // Если это было последнее простое слово, и его не удалось найти в словаре, заносим в массив
                        // слов значение dictionary::undefined
                        if (--words_count == 0)
                        {
                            // Если вдруг в слове был дефис, попробуем найти кусок слова до дефиса: иногда к слову при-
                            // соедниняют частицу (например: был-то он молод. Запишем слово просто как "был")
                            const unsigned char* hyphen_p;
                            if ((hyphen_p = std::find(
                                     sentence+last_words.front().first,
                                     sentence+last_words.front().second,
                                     '-')) != sentence+last_words.front().second)
                            {
                                id = d.find(sentence+last_words.front().first, hyphen_p);
                                words.push_back(id == dictionary::absent ? dictionary::undefined : d.get(id));
                                if (id != dictionary::absent) ++lexems[words.back().id()];
                            }
                            else
                            {
                                words.push_back(dictionary::undefined);
                            }
                            words_count = 1;
                            total_word_length += last_words.front().second - last_words.front().first;
                            break;
                        }
                    }
                    else
                    {
                        words.push_back(d.get(id));
                        ++lexems[words.back().id()];
                        total_word_length += last_words[words_count-1].second - last_words.front().first;
                        break;
                    }
                }

                // Если в словаре нашлись все простые слова как одно составное, очищаем вектор целиком
                if (words_count == last_words.size())
                {
                    last_words.clear();
                }
                // Иначе производим сдвиг на необходимое количество слов
                else
                {
                    for (uint i = words_count; i < last_words.size(); ++i)
                    {
                        last_words[i-words_count] = last_words[i];
                    }
                    last_words.erase(last_words.end()-words_count);
                }

                // Если это был пробел, дальнейшей очистки после записи максимально длинного слова не требуется
                if (ENCODING[c] & CHAR_TYPES::WHITE_CHAR)
                {
                    break;
                }
            }

            saved_words_need_erase = false;
        }

        // Останавливаемся при достижении пороговой длины для анализа
        if (chars_count >= length_treshold) break;
        // Вычисляем количество слов в считанном предложении
        sentence_lengths.push_back(static_cast<uint>(words.size())-words_total);
        // Прибавляем полученную длину к накопленной сумме длин
        total_length += sentence_lengths.back();
    }

    // Вычисляем среднюю длину предложения в словах
    average_sentence_length = total_length/static_cast<double>(sentence_lengths.size());
    // Вычисляем среднюю длину слова
    average_word_length = total_word_length/static_cast<double>(words.size());

    log::d() << TAG << "Процедура разбития на слова завершена, слов считано: " << words.size();
}

void text::get_n_grams(const uint n, bool words_only)
{
    log::d() << TAG << "Начало сбора " << n << "-грамм" << (words_only ? " (только буквы)" : "");

    // Мы собираем n-граммы только для n = 2, ..., 8
    if (n < 2 || n > 4) return;

    // Текущее значение n-граммы (младшие n бит)
    uint n_gram = 0;
    // Счётчик считанных символов
    uint k = 0;
    // Если есть фильтр по словам, используем счётчик того, сколько следующих n-грамм надо пропустить
    uint skip_some = 0;

    for (const unsigned char* sentence: sentences)
    {
        // Цикл по всем n-граммам предложения. В предложении из r символов встречается r-n+1 n-грамм
        for (uint i = 0; const unsigned char c = sentence[i]; ++i)
        {
            // Считали на символ больше
            ++k;
            // Завершаем анализ по достижении пороговой длины
            if (k >= length_treshold) return;

            // Получаем новую n-грамму. n-1 крайних правых символов в ней остаются неизменными, сдвигаем их на 1 байт.
            // Затем добавляем новый полученный символ
            (n_gram <<= 8) |= c;

            // Если пока не считали n символов - сохранять нечего
            if (k < n) continue;

            // Если допускаем только символы, из которых состоят слова, а текущий символ оказался не буквой, не дефисом
            // и не пробелом, не добавляем данную n-грамму в список
            if (words_only)
            {
                if (!(ENCODING[c] & CHAR_TYPES::WORD_CHAR))
                {
                    skip_some = n;
                    continue;
                }
                else
                {
                    if (skip_some > 0) --skip_some;
                }
            }

            // Увеличиваем вхождение n-граммы на одно. n-грамма находится в массиве n_grams по индексу n-2.
            // Учитываем при этом только младшие n байт
            if (!skip_some)
            {
                if (words_only) ++n_grams_words[n-2][n_gram & (0xFFFFFFFFu >> ((8-n)*8))];
                else ++n_grams[n-2][n_gram & (0xFFFFFFFFu >> ((8-n)*8))];
            }
        }

        // Предложение кончилось. Считаем, что предложения разделены пробелом, поэтому добавляем в n-грамму пробел
        (n_gram <<= 8) |= ' ';
        if (++k >= n && !skip_some && !words_only)
        {
            ++n_grams[n-2][n_gram & (0xFFFFFFFFu >> ((8-n)*8))];
        }
    }

    log::d() << TAG << "Конец сбора " << n << "-грамм" << (words_only ? " (только буквы)" : "")
             << ", собрано: " << (words_only ? n_grams_words : n_grams)[n-2].size();

}

void text::get_part_of_speech_distribution()
{
    log::d() << TAG << "Получение распределения частей речи";

    for (word& w: words)
    {
        if (w.part_of_speech() != static_cast<uint>(PARTS_OF_SPEECH::UNKNOWN))
        {
            ++part_of_speech_distribution[w.part_of_speech()];
        }
    }

    log::d() << TAG << "Распределение частей речи получено";
}

void text::get_word_lengths_distribution()
{
    log::d() << TAG << "Получение распределения слов по длинам";

    for (word& w: words)
    {
        ++word_lengths_distribution[w.length()];
    }

    log::d() << TAG << "Распределение слов по длинам получено";
}

void text::get_sentence_lengths_distribution()
{
    log::d() << TAG << "Получение распределения предложений по длинам слов";

    for (uint l: sentence_lengths)
    {
        // Предложения группируются длиной по 3
        if (l > 0)
        {
            ++sentence_lengths_distribution[l/3+1];
        }
    }

    log::d() << TAG << "Распределение предложений по длинам слов получено";
}

void text::get_part_of_speech_pairs_head_distribution()
{
    log::d() << TAG << "Получение распределения пар частей речи в начале предложения";

    // Индекс текущего слова
    uint current_word = 0;

    for (uint l: sentence_lengths)
    {
        // Если длина предложения больше или равна 2 слова, включаем пару первых двух слов в распределение
        if (l >= 2)
        {
            try
            {
                uint part_of_speech1 = words.at(current_word).part_of_speech();
                uint part_of_speech2 = words.at(current_word+1).part_of_speech();
                if (part_of_speech1 != static_cast<uint>(PARTS_OF_SPEECH::UNKNOWN) &&
                        part_of_speech2 != static_cast<uint>(PARTS_OF_SPEECH::UNKNOWN))
                {
                    ++part_of_speech_pairs_head_distribution[(part_of_speech1 << 4)|part_of_speech2];
                }
            }
            catch (std::out_of_range&)
            {
                log::e() << TAG << " Произошёл выход за границы массива слов, индекс: " << current_word;
            }
        }
        current_word += l;
    }

    log::d() << TAG << "Распределение пар частей речи в начале предложения получено";
}

void text::get_part_of_speech_pairs_tail_distribution()
{
    log::d() << TAG << "Получение распределения пар частей речи в конце предложения";

    // Индекс текущего слова
    uint current_word = 0;

    for (uint l: sentence_lengths)
    {
        // Если длина предложения больше или равна 2 слова, включаем пару последних двух слов в распределение
        if (l >= 2)
        {
            try
            {
                uint part_of_speech1 = words.at(current_word+l-2).part_of_speech();
                uint part_of_speech2 = words.at(current_word+l-1).part_of_speech();
                if (part_of_speech1 != static_cast<uint>(PARTS_OF_SPEECH::UNKNOWN) &&
                        part_of_speech2 != static_cast<uint>(PARTS_OF_SPEECH::UNKNOWN))
                {
                    ++part_of_speech_pairs_tail_distribution[(part_of_speech1 << 4)|part_of_speech2];
                }
            }
            catch (std::out_of_range&)
            {
                log::e() << TAG << " Произошёл выход за границы массива слов, индекс: " << current_word;
            }
        }
        current_word += l;
    }

    log::d() << TAG << "Распределение пар частей речи в конце предложения получено";
}

void text::get_part_of_speech_trios_head_distribution()
{
    log::d() << TAG << "Получение распределения троек частей речи в начале предложения";

    // Индекс текущего слова
    uint current_word = 0;

    for (uint l: sentence_lengths)
    {
        // Если длина предложения больше или равна 3 слова, включаем пару первых трёх слов в распределение
        if (l >= 3)
        {
            try
            {
                uint part_of_speech1 = words.at(current_word).part_of_speech();
                uint part_of_speech2 = words.at(current_word+1).part_of_speech();
                uint part_of_speech3 = words.at(current_word+2).part_of_speech();
                if (part_of_speech1 != static_cast<uint>(PARTS_OF_SPEECH::UNKNOWN) &&
                        part_of_speech2 != static_cast<uint>(PARTS_OF_SPEECH::UNKNOWN) &&
                        part_of_speech3 != static_cast<uint>(PARTS_OF_SPEECH::UNKNOWN))
                {
                    ++part_of_speech_trios_head_distribution[
                            (part_of_speech1 << 8) | (part_of_speech2 << 4) | part_of_speech3
                            ];
                }
            }
            catch (std::out_of_range&)
            {
                log::e() << TAG << " Произошёл выход за границы массива слов, индекс: " << current_word;
            }
        }
        current_word += l;
    }

    log::d() << TAG << "Распределение троек частей речи в начале предложения получено";
}

void text::get_part_of_speech_trios_tail_distribution()
{
    log::d() << TAG << "Получение распределения троек частей речи в конце предложения";

    // Индекс текущего слова
    uint current_word = 0;

    for (uint l: sentence_lengths)
    {
        // Если длина предложения больше или равна 3 слова, включаем пару последних трёх слов в распределение
        if (l >= 3)
        {
            try
            {
                uint part_of_speech1 = words.at(current_word+l-3).part_of_speech();
                uint part_of_speech2 = words.at(current_word+l-2).part_of_speech();
                uint part_of_speech3 = words.at(current_word+l-1).part_of_speech();
                if (part_of_speech1 != static_cast<uint>(PARTS_OF_SPEECH::UNKNOWN) &&
                        part_of_speech2 != static_cast<uint>(PARTS_OF_SPEECH::UNKNOWN) &&
                        part_of_speech3 != static_cast<uint>(PARTS_OF_SPEECH::UNKNOWN))
                {
                    ++part_of_speech_trios_tail_distribution[
                            (part_of_speech1 << 8) | (part_of_speech2 << 4) | part_of_speech3
                            ];
                }
            }
            catch (std::out_of_range&)
            {
                log::e() << TAG << " Произошёл выход за границы массива слов, индекс: " << current_word;
            }
        }
        current_word += l;
    }

    log::d() << TAG << "Распределение троек частей речи в конце предложения получено";
}

void text::get_word_profile()
{
    log::d() << TAG << "Получение словарного профиля";

    for (auto p: lexems) word_profile[p.second] += p.second;

    log::d() << TAG << "Словарный профиль получен";
}

void text::get_case_distribution()
{
    log::d() << TAG << "Получение распределения слов по падежам";

    for (word& w: words)
    {
         if (w.case_t() != static_cast<uint>(CASE::UNKNOWN))
         {
             ++case_distribution[w.case_t()];
         }
    }

    log::d() << TAG << "Распределение слов по падежам получено";
}

void text::get_auxiliary_words_rate()
{
    log::d() << TAG << "Получение доли служебных частей речи";

    // Счётчик частей речи
    uint auxiliary_words_count = 0;

    for (word& w: words)
    {
        if (w.is_auxiliary()) ++auxiliary_words_count;
    }

    auxiliary_words_rate = static_cast<double>(auxiliary_words_count)/words.size();

    log::d() << TAG << "Доля служебных частей речи получена";
}

void text::get_words_variery_rate()
{
    log::d() << TAG << "Получение коэффициента словарного состава";

    words_variery_rate = static_cast<double>(lexems.size())/words.size();

    log::d() << TAG << "Коэффициент словарного состава получен";
}

void text::get_selfness_rate()
{
    log::d() << TAG << "Получение доли слов с наличием возвратного суффикса";

    uint selfness_candidates = 0, selfness_words = 0;
    for (word& w: words)
    {
        uint part_of_speech = w.part_of_speech();
        if (part_of_speech == static_cast<uint>(PARTS_OF_SPEECH::PARTICIPLE) ||
                part_of_speech == static_cast<uint>(PARTS_OF_SPEECH::VERB) ||
                part_of_speech == static_cast<uint>(PARTS_OF_SPEECH::VERB_PARTICIPLE))
        {
            ++selfness_candidates;
            if (w.is_self()) ++selfness_words;
        }
    }

    selfness_rate = selfness_candidates == 0 ? 0. : static_cast<double>(selfness_words)/selfness_candidates;

    log::d() << TAG << "Доля слов с наличием возвратного суффикса получена";
}

void text::cut_distribution(std::map<uint, uint> &d)
{
    // Обрезаем распределения только более 1000 элементов
    if (d.size() <= OPTIMAL_DISTRIBUTION_SIZE) return;

    std::multimap<uint, uint> flipped_map;

    // Меняем метсами ключи и значения - получаем отсортированное распределение
    std::transform(d.begin(), d.end(), std::inserter(flipped_map, flipped_map.begin()), [&] (std::pair<uint, uint> x)
    {
        return std::pair<uint, uint>{x.second, x.first};
    });

    // Записываем последние 1000 элементов назад
    d.clear();
    uint count = 0;
    for (auto it = flipped_map.rbegin(); it != flipped_map.rend(); ++it)
    {
        d[it->second] =  it->first;
        if (++count >= OPTIMAL_DISTRIBUTION_SIZE) break;
    }

    // Очищаем служебную карту
    flipped_map.clear();
}

void text::print_info_to(const char *path)
{
    if (info_stream.is_open())
    {
        info_stream.close();
    }

    info_stream.open(path);
}

void text::print_sentences(const char *delim_str)
{
    if (!info_stream.is_open() || info_stream.bad())
    {
        log::e() << TAG << "Поток для печати предложений недоступен";
        return;
    }

    info_stream << "=== " << u(A::S) << u(A::p) << u(A::i) << u(A::s) << u(A::o) << u(A::k)
                << ' ' << u(A::p) << u(A::r) << u(A::e) << u(A::d) << u(A::l) << u(A::o)
                << u(A::zh) << u(A::e) << u(A::n) << u(A::i) << u(A::j) << " ===\n\n";

    for (const unsigned char* sentence: sentences)
    {
        if (delim_str != nullptr)
        {
            info_stream << delim_str;
        }

        info_stream << sentence << '\n';
    }
}

void text::print_words()
{
    if (!info_stream.is_open() || info_stream.bad())
    {
        log::e() << TAG << "Поток для печати слов недоступен";
        return;
    }

    info_stream << "=== " << u(A::S) << u(A::p) << u(A::i) << u(A::s) << u(A::o) << u(A::k)
                << ' ' << u(A::s) << u(A::l) << u(A::o) << u(A::v) << " ===\n\n";

    dictionary& d = dictionary::get();

    for (uint i = 0; i < words.size(); ++i)
    {
        info_stream << " (" << i << ") ";
        d.print(info_stream, words[i], false);
    }

    info_stream << '\n';
}

void text::print_n_grams(uint n, bool words_only)
{
    if (!info_stream.is_open() || info_stream.bad())
    {
        log::e() << TAG << "Поток для печати n-грамм недоступен";
        return;
    }

    info_stream << "=== " << u(A::S) << u(A::p) << u(A::i) << u(A::s) << u(A::o) << u(A::k)
                << ' ' << n << '-' << u(A::g) << u(A::r) << u(A::a) << u(A::m) << u(A::m);

    if (words_only)
    {
        info_stream << '(' << u(A::b) << u(A::u) << u(A::k) << u(A::v) << u(A::yu) << ')';
    }

    info_stream << " ===\n\n";

    for (auto& p: words_only ? n_grams_words[n-2] : n_grams[n-2])
    {
        for (uint j = n-1; j != 0xFFFFFFFFu; --j)
        {
            info_stream << static_cast<unsigned char>((p.first & (0xFFu << j*8)) >> j*8);
        }
        info_stream << ": " << p.second << '\n';
    }
}

void text::print_distributions()
{
    if (!info_stream.is_open() || info_stream.bad())
    {
        log::e() << TAG << "Поток для печати распределений недоступен";
        return;
    }

    // Распределение частей речи

    info_stream << "=== " << u(A::R) << u(A::a) << u(A::s) << u(A::p) << u(A::r) << u(A::e) << u(A::d) << u(A::e)
                << u(A::l) << u(A::e) << u(A::n) << u(A::i) << u(A::e) << ' ' << u(A::ch) << u(A::a) << u(A::s)
                << u(A::t) << u(A::e) << u(A::j) << ' ' << u(A::r) << u(A::e) << u(A::ch) << u(A::i) << " ===\n\n";

    for (auto& p: part_of_speech_distribution)
    {
        ustring s = part_of_speech(p.first);
        info_stream << *reinterpret_cast<std::string*>(&s) << ": " << p.second << '\n';
    }

    separator();

    // Распределение слов по длинам

    info_stream << "=== " << u(A::R) << u(A::a) << u(A::s) << u(A::p) << u(A::r) << u(A::e) << u(A::d) << u(A::e)
                << u(A::l) << u(A::e) << u(A::n) << u(A::i) << u(A::e) << ' ' << u(A::d) << u(A::l) << u(A::i)
                << u(A::n) << ' ' << u(A::s) << u(A::l) << u(A::o) << u(A::v) << " ===\n\n";

    for (auto& p: word_lengths_distribution)
    {
        info_stream << p.first << ": " << p.second << '\n';
    }

    separator();

    // Распределение предложений по длинам

    info_stream << "=== " << u(A::R) << u(A::a) << u(A::s) << u(A::p) << u(A::r) << u(A::e) << u(A::d) << u(A::e)
                << u(A::l) << u(A::e) << u(A::n) << u(A::i) << u(A::e) << ' ' << u(A::d) << u(A::l) << u(A::i)
                << u(A::n) << ' ' << u(A::p) << u(A::r) << u(A::e) << u(A::d) << u(A::l) << u(A::o) << u(A::zh)
                << u(A::e) << u(A::n) << u(A::i) << u(A::j) << " ===\n\n";

    for (auto& p: sentence_lengths_distribution)
    {
        info_stream << (p.first*3)-2 << '-' << (p.first*3) << ": " << p.second << '\n';
    }

    separator();

    // Распределение пар частей речи в начале предложения

    info_stream << "=== " << u(A::R) << u(A::a) << u(A::s) << u(A::p) << u(A::r) << u(A::e) << u(A::d) << u(A::e)
                << u(A::l) << u(A::e) << u(A::n) << u(A::i) << u(A::e) << ' ' << u(A::p) << u(A::a) << u(A::r)
                << ' ' << u(A::ch) << u(A::a) << u(A::s) << u(A::t) << u(A::e) << u(A::j) << ' ' << u(A::r) << u(A::e)
                << u(A::ch) << u(A::i) << ' ' << u(A::v) << ' ' << u(A::n) << u(A::a) << u(A::ch) << u(A::a)
                << u(A::l) << u(A::e) << " ===\n\n";

    for (auto& p: part_of_speech_pairs_head_distribution)
    {
        uint c1 = (p.first&0x000000F0u) >> 4;
        uint c2 = p.first&0x0000000Fu;
        ustring s1 = part_of_speech(c1);
        ustring s2 = part_of_speech(c2);
        info_stream << *reinterpret_cast<std::string*>(&s1) << ','
                    << *reinterpret_cast<std::string*>(&s2) << ": " << p.second << '\n';
    }

    separator();

    // Распределение пар частей речи в конце предложения

    info_stream << "=== " << u(A::R) << u(A::a) << u(A::s) << u(A::p) << u(A::r) << u(A::e) << u(A::d) << u(A::e)
                << u(A::l) << u(A::e) << u(A::n) << u(A::i) << u(A::e) << ' ' << u(A::p) << u(A::a) << u(A::r)
                << ' ' << u(A::ch) << u(A::a) << u(A::s) << u(A::t) << u(A::e) << u(A::j) << ' ' << u(A::r) << u(A::e)
                << u(A::ch) << u(A::i) << ' ' << u(A::v) << ' ' << u(A::k) << u(A::o) << u(A::n) << u(A::ts)
                << u(A::e) << " ===\n\n";

    for (auto& p: part_of_speech_pairs_tail_distribution)
    {
        uint c1 = (p.first&0x000000F0u) >> 4;
        uint c2 = p.first&0x0000000Fu;
        ustring s1 = part_of_speech(c1);
        ustring s2 = part_of_speech(c2);
        info_stream << *reinterpret_cast<std::string*>(&s1) << ','
                    << *reinterpret_cast<std::string*>(&s2) << ": " << p.second << '\n';
    }

    separator();

    // Распределение троек частей речи в начале предложения

    info_stream << "=== " << u(A::R) << u(A::a) << u(A::s) << u(A::p) << u(A::r) << u(A::e) << u(A::d) << u(A::e)
                << u(A::l) << u(A::e) << u(A::n) << u(A::i) << u(A::e) << ' ' << u(A::t) << u(A::r) << u(A::o)
                << u(A::e) << u(A::k) << ' ' << u(A::ch) << u(A::a) << u(A::s) << u(A::t) << u(A::e) << u(A::j) << ' '
                << u(A::r) << u(A::e) << u(A::ch) << u(A::i) << ' ' << u(A::v) << ' ' << u(A::n) << u(A::a) << u(A::ch)
                << u(A::a) << u(A::l) << u(A::e) << " ===\n\n";

    for (auto& p: part_of_speech_trios_head_distribution)
    {
        uint c1 = (p.first&0x00000F00u) >> 8;
        uint c2 = (p.first&0x000000F0u) >> 4;
        uint c3 = p.first&0x0000000Fu;
        ustring s1 = part_of_speech(c1);
        ustring s2 = part_of_speech(c2);
        ustring s3 = part_of_speech(c3);
        info_stream << *reinterpret_cast<std::string*>(&s1) << ','
                    << *reinterpret_cast<std::string*>(&s2) << ','
                    << *reinterpret_cast<std::string*>(&s3) << ": " << p.second << '\n';
    }

    separator();

    // Распределение троек частей речи в конце предложения

    info_stream << "=== " << u(A::R) << u(A::a) << u(A::s) << u(A::p) << u(A::r) << u(A::e) << u(A::d) << u(A::e)
                << u(A::l) << u(A::e) << u(A::n) << u(A::i) << u(A::e) << ' ' << u(A::t) << u(A::r) << u(A::o)
                << u(A::e) << u(A::k) << ' ' << u(A::ch) << u(A::a) << u(A::s) << u(A::t) << u(A::e) << u(A::j) << ' '
                << u(A::r) << u(A::e) << u(A::ch) << u(A::i) << ' ' << u(A::v) << ' ' << u(A::k) << u(A::o) << u(A::n)
                << u(A::ts) << u(A::e) << " ===\n\n";

    for (auto& p: part_of_speech_trios_tail_distribution)
    {
        uint c1 = (p.first&0x00000F00u) >> 8;
        uint c2 = (p.first&0x000000F0u) >> 4;
        uint c3 = p.first&0x0000000Fu;
        ustring s1 = part_of_speech(c1);
        ustring s2 = part_of_speech(c2);
        ustring s3 = part_of_speech(c3);
        info_stream << *reinterpret_cast<std::string*>(&s1) << ','
                    << *reinterpret_cast<std::string*>(&s2) << ','
                    << *reinterpret_cast<std::string*>(&s3) << ": " << p.second << '\n';
    }

    separator();

    // Словарный профиль

    info_stream << "=== " << u(A::S) << u(A::l) << u(A::o) << u(A::v) << u(A::a) << u(A::r) << u(A::n) << u(A::y)
                << u(A::j) << ' ' << u(A::p) << u(A::r) << u(A::o) << u(A::f) << u(A::i) << u(A::l) << u(A::soft_sign)
                << " ===\n\n";


    for (auto& p: word_profile)
    {
        info_stream << p.first << ": " << p.second << '\n';
    }

    separator();

    // Распределение по падежам

    info_stream << "=== " << u(A::R) << u(A::a) << u(A::s) << u(A::p) << u(A::r) << u(A::e) << u(A::d) << u(A::e)
                << u(A::l) << u(A::e) << u(A::n) << u(A::i) << u(A::e) << ' ' << u(A::p) << u(A::a) << u(A::d)
                << u(A::e) << u(A::zh) << u(A::e) << u(A::j) << " ===\n\n";

    for (auto& p: case_distribution)
    {
        ustring s = case_name(p.first);
        info_stream << *reinterpret_cast<std::string*>(&s) << ": " << p.second << '\n';
    }
}

void text::print_primitives()
{
    info_stream << "=== " << u(A::P) << u(A::r) << u(A::i) << u(A::m) << u(A::i) << u(A::t) << u(A::i) << u(A::v)
                << u(A::n) << u(A::y) << u(A::e) << ' ' << u(A::h) << u(A::a) << u(A::r) << u(A::a) << u(A::k)
                << u(A::t) << u(A::e) << u(A::r) << u(A::i) << u(A::s) << u(A::t) << u(A::i) << u(A::k) << u(A::i)
                << " ===\n\n";

    info_stream << u(A::S) << u(A::l) << u(A::u) << u(A::zh) << ".: " << auxiliary_words_rate << '\n';
    info_stream << u(A::K) << u(A::S) << u(A::S) << ": " << words_variery_rate << '\n';
    info_stream << u(A::V) << u(A::o) << u(A::z) << u(A::v) << ".: " << selfness_rate << '\n';

}

void text::print_all(bool characterstics_only)
{
    if (!info_stream.is_open() || info_stream.bad())
    {
        log::e() << TAG << "Поток для печати информации о тексте недоступен";
        return;
    }

    log::d() << TAG << "Начинается печать информации о тексте " << file_path;

    info_stream << u(A::T) << u(A::e) << u(A::k) << u(A::s) << u(A::t) << ": " << file_path << '\n';
    info_stream << u(A::A) << u(A::v) << u(A::t) << u(A::o) << u(A::r) << ": " << author << '\n';
    info_stream << u(A::N) << u(A::a) << u(A::z) << u(A::v) << u(A::a)
                << u(A::n) << u(A::i) << u(A::e) << ": " << title << '\n';

    if (characterstics_only) goto characteristics;

    separator();

    info_stream << "=== " << u(A::O) << u(A::b)<< u(A::sch) << u(A::a) << u(A::ya) << ' ' << u(A::i) << u(A::n)
                << u(A::f) << u(A::o) << u(A::r) << u(A::m) << u(A::a) << u(A::ts) << u(A::i) << u(A::ya) << " ===\n\n";

    info_stream << u(A::S) << u(A::l) << u(A::o) << u(A::v) << ": " << words.size() << '\n';
    info_stream << u(A::P) << u(A::r) << u(A::e) << u(A::d) << u(A::l) << u(A::o)
                << u(A::zh) << u(A::e) << u(A::n) << u(A::i) << u(A::j) << ": " << sentences.size() << '\n';

    info_stream << u(A::S) << u(A::r) << u(A::e) << u(A::d) << u(A::n) << u(A::ya) << u(A::ya) << ' '
                << u(A::d) << u(A::l) << u(A::i) << u(A::n) << u(A::a) << ' '
                << u(A::s) << u(A::l) << u(A::o) << u(A::v) << u(A::a) << ": " << average_word_length << '\n';

    info_stream << u(A::S) << u(A::r) << u(A::e) << u(A::d) << u(A::n) << u(A::ya) << u(A::ya) << ' '
                << u(A::d) << u(A::l) << u(A::i) << u(A::n) << u(A::a) << ' '
                << u(A::p) << u(A::r) << u(A::e) << u(A::d) << u(A::l) << u(A::o)
                << u(A::zh) << u(A::e) << u(A::n) << u(A::i) << u(A::ya) << ": " << average_sentence_length << '\n';

    separator();
    print_sentences();
    separator();
    print_words();
    separator();
characteristics:
    print_n_grams(2, false);
    separator();
    print_n_grams(3, false);
    separator();
    print_n_grams(4, false);
    separator();
    print_n_grams(2, true);
    separator();
    print_n_grams(3, true);
    separator();
    print_n_grams(4, true);
    separator();
    print_distributions();
    separator();
    print_primitives();

    log::d() << TAG << "Информация о тексте " << file_path << " напечатана";
}

void text::write_distribution_as_binary(std::map<uint, uint> distribution, std::ofstream& s)
{
    write_big_endian(static_cast<uint>(distribution.size()), s); // L
    for (auto& p: distribution)
    {
        // V
        write_big_endian(p.first, s);
        write_big_endian(p.second, s);
    }
}

void text::write_primitive_as_binary(double primitive, std::ofstream &s)
{
    write_big_endian(static_cast<uint>(sizeof (double)), s); // L
    char* byte_array = reinterpret_cast<char*>(&primitive);
    std::ostream_iterator<char> it(s);
    std::copy(byte_array, byte_array+sizeof(double), it); // V
}

void text::save_as_binary(const char *to)
{
    std::ofstream s;
    s.open(to, std::ios_base::out | std::ios_base::binary);

    if (!s.is_open() || s.bad())
    {
        log::e() << TAG << "Невозможно записать характеристики текста в " << to;
    }

    // Запись характеристик в формате Length, Value

    // Запись частот N-грамм
    for (uint i = 0; i < 3; ++i) write_distribution_as_binary(n_grams[i], s);
    // Запись частот буквенных N-грамм
    for (uint i = 0; i < 3; ++i) write_distribution_as_binary(n_grams_words[i], s);
    // Запись распределения частей речи
    write_distribution_as_binary(part_of_speech_distribution, s);
    // Запись распределения длин слов
    write_distribution_as_binary(word_lengths_distribution, s);
    // Запись распределения длин предложений
    write_distribution_as_binary(sentence_lengths_distribution, s);
    // Запись распределения пар частей речи в начале предложения
    write_distribution_as_binary(part_of_speech_pairs_head_distribution, s);
    // Запись распределения пар частей речи в конце предложения
    write_distribution_as_binary(part_of_speech_pairs_tail_distribution, s);
    // Запись распределения троек частей речи в начале предложения
    write_distribution_as_binary(part_of_speech_trios_head_distribution, s);
    // Запись распределения троек частей речи в конце предложения
    write_distribution_as_binary(part_of_speech_trios_tail_distribution, s);
    // Запись словарного профиля
    write_distribution_as_binary(word_profile, s);
    // Запись распределения падежей
    write_distribution_as_binary(case_distribution, s);
    // Запись доли служебных слов
    write_primitive_as_binary(auxiliary_words_rate, s);
    // Запись КСС
    write_primitive_as_binary(words_variery_rate, s);
    // Запись доли слов с возвратным суффиксом
    write_primitive_as_binary(selfness_rate, s);

    s.close();
}

void text::load_distribution(std::map<uint, uint>& distribution, uint data_size, std::ifstream& source)
{
    for (uint i = 0; i < data_size; ++i)
    {
        try
        {
            uint key = read_big_endian(source);
            uint value = read_big_endian(source);
            distribution[key] = value;
        }
        catch (std::ios_base::failure& e)
        {
            log::e() << TAG << "Произошла неожиданная ошибка при считывании распределения из файла на диске. "
                     << "Распределение сохранено не полностью: " << e.what();
        }
    }
}

void text::load_primitive(double& value, std::ifstream &source)
{
    try
    {
        source.read(reinterpret_cast<char*>(&value), sizeof(double));
    }
    catch (std::ios_base::failure& e)
    {
        log::e() << TAG << "Произошла неожиданная ошибка при считывании примитивной характеристики из файда на диске. "
                 << "Характеристика не считана: " << e.what();
    }
}

std::string text::get_author()
{
    return author;
}

std::map<uint,uint>& text::get_distribution_by_number(uint number)
{
    switch (number)
    {
        case 1: return n_grams[0];
        case 2: return n_grams[1];
        case 3: return n_grams[2];
        case 4: return n_grams_words[0];
        case 5: return n_grams_words[1];
        case 6: return n_grams_words[2];
        case 7: return part_of_speech_distribution;
        case 8: return word_lengths_distribution;
        case 9: return sentence_lengths_distribution;
        case 10: return part_of_speech_pairs_head_distribution;
        case 11: return part_of_speech_pairs_tail_distribution;
        case 12: return part_of_speech_trios_head_distribution;
        case 13: return part_of_speech_trios_tail_distribution;
        case 14: return word_profile;
        case 15: return case_distribution;
        // Числа больше 15 передавать нельзя
        default: return n_grams[0];
    }
}

double text::get_primitive_by_number(uint number)
{
    switch (number)
    {
        case 16: return auxiliary_words_rate;
        case 17: return words_variery_rate;
        case 18: return selfness_rate;
        // Числа кроме 16, 17, 18 передавать нельзя
        default: return 0;
    }
}

#endif // TEXT_CPP
