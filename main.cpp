#include <iostream>
#include "ta/env.hpp"
#include "ta/utils/log.hpp"
#include "ta/utils/utils.hpp"
#include "ta/grammar/word.hpp"
#include "ta/grammar/text.hpp"
#include "ta/tools/dictionary.hpp"
#include "ta/tools/analysis.hpp"

using namespace std;
using namespace ta;
using namespace ta::tools;
using namespace ta::grammar;
using namespace ta::utils;

int main()
{
    env env;
    log::d() << TAG << "Программа запущена";
    //ta::utils::parse_hagen_m_dictionary(env.paths.hagen_m_dictionary, env.paths.binary_dictionary);

    try
    {
        analysis a("/usr/local/russian_dictionary/analysis_step1",
                   "/usr/local/russian_dictionary/analysis_step2",
                   "/usr/local/russian_dictionary/analysis_step3");
        //a.analyse_all_texts_from_path("/usr/local/russian_dictionary/texts_set");
        //a.compare_all(false);
        a.get_n_texts(4);
        a.get_n_texts(5);
        a.get_n_texts(6);
        a.get_n_texts(7);
        a.get_n_texts(8);
        a.get_n_texts(9);
        a.get_n_texts(10);
        //a.check_h_precision(4);
        //a.check_h_precision(5);
        //a.check_h_precision(6);
        //a.check_h_precision(7);
//        a.check_h_precision(8);
//        a.check_h_precision(9);
//        a.check_h_precision(10);

        //a.h_precision_for_n_texts(4);
        //a.h_precision_for_n_texts(5);
        //a.h_precision_for_n_texts(6);
        //a.h_precision_for_n_texts(7);










//        text t = text("/usr/local/russian_dictionary/texts_set/24.txt",
//                      text::scan_mode::AUTHOR_DOT_TITLE,
//                      text::analysis::SKIP_TITLES);
//        t.analyze(7000, true);
//        t.print_info_to("/usr/local/russian_dictionary/info/24.txt");
//        t.print_all();
//        t.save_as_binary("/usr/local/russian_dictionary/info/24b.txt");
//        text tb("/usr/local/russian_dictionary/analysis_step1/binary/640000/32-34-16-24-2332-61-27-16-20-32-19/172.txt", text::scan_mode::BINARY_CHARACTERISTICS);
//        tb.print_info_to("/usr/local/russian_dictionary/info/172fb.txt");
//        tb.print_all(true);

    }
    catch (invalid_argument& e)
    {
        log::e() << TAG << e.what();
    }
    catch (ios_base::failure& e)
    {
        log::e() << TAG << e.what();
    }
    catch (bad_alloc&)
    {
        log::e() << TAG << "Не удаётся выделить достаточно памяти для хранения словаря";
    }
    catch (exception& e)
    {
        log::e() << TAG << e.what();
    }


    return 0;
}
